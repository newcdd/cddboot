package com.botw.aml.sieve.util;

import com.botw.aml.sieve.eval.util.TreeNode;
import org.junit.Test;

/**
 * Created by krasnd52 on 12/4/15.
 */
public class GeneralTreeTest {

    @Test
    public void testGetRoot() throws Exception {
        TreeNode<String> cio = new TreeNode<String>("Kirsten")
                .setFirstChild(new TreeNode<String>("David")
                        .setFirstChild(new TreeNode<String>("Dmitriy"))
                        .setNextSibling(new TreeNode<String>("Mike")
                                .setNextSibling(new TreeNode<String>("Tibor")
                                        .setNextSibling(new TreeNode<String>("Sree")
                                                .setFirstChild(new TreeNode<String>("Erik")
                                                        .setNextSibling(new TreeNode<String>("Jacob"))
                                                        .setFirstChild(new TreeNode<String>("Carolin")
                                                                .setFirstChild(new TreeNode<String>("Akshay")
                                                                        .setNextSibling(new TreeNode<String>("Srinivas")))))))));

        System.out.println(cio.toString());
    }
}