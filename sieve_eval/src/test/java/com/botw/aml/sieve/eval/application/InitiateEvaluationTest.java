package com.botw.aml.sieve.eval.application;

import com.botw.aml.sieve.eval.application.commands.InitiateEvaluationCommand;
import com.botw.aml.sieve.eval.domain.model.eval.Evaluation;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationProgress;
import org.junit.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static com.botw.aml.sieve.common.matchers.StringMatchesUUIDPattern.matchesThePatternOfAUUID;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by krasnd52 on 12/14/15.
 */
public class InitiateEvaluationTest {

    @Test
    public void should_return_evaluation_id() throws Exception {
        EvaluationApplicationService service = new EvaluationApplicationService();
        InitiateEvaluationCommand command = new InitiateEvaluationCommand(
                "globalId",
                null,
                null,
                null);
     /*   Evaluation systemUnderTest = service.initiateEvaluation(command);

        assertThat(systemUnderTest.getEvaluationId(), is(notNullValue()));
        assertThat(systemUnderTest.getEvaluationId().id(), is(matchesThePatternOfAUUID()));
        */
    }

    @Test
    public void should_return_eta_of_5_sec() throws Exception {
        EvaluationApplicationService service = new EvaluationApplicationService();
        InitiateEvaluationCommand command = new InitiateEvaluationCommand(
                "globalId",
                null,
                null,
                null);
    /*    Evaluation evaluation = service.initiateEvaluation(command);
        EvaluationProgress systemUnderTest = evaluation.getProgress();

        assertThat(5L, equalTo(systemUnderTest.getWaitEstimateInSeconds()));
        */
    }

   // @Test
    public void should_return_url_for_progress_tracking() throws Exception {
        throw new NotImplementedException();

    }

}