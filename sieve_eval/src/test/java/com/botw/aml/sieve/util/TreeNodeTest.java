package com.botw.aml.sieve.util;

import com.botw.aml.sieve.eval.util.TreeNode;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by krasnd52 on 12/4/15.
 */
public class TreeNodeTest {
    private TreeNode<String> cio;

    @Before
    public void setUp() throws Exception {
        cio = new TreeNode<String>("Kirsten")
                .setFirstChild(new TreeNode<String>("David")
                        .setFirstChild(new TreeNode<String>("Dmitriy"))
                        .setNextSibling(new TreeNode<String>("Mike")
                                .setNextSibling(new TreeNode<String>("Tibor")
                                        .setNextSibling(new TreeNode<String>("Sree")
                                                .setFirstChild(new TreeNode<String>("Erik")
                                                        .setNextSibling(new TreeNode<String>("Jacob"))
                                                        .setFirstChild(new TreeNode<String>("Carolin")
                                                                .setFirstChild(new TreeNode<String>("Akshay")
                                                                        .setNextSibling(new TreeNode<String>("Srinivas")))))))));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testIsLeaf() throws Exception {
        TreeNode<String> root = new TreeNode<String>("root");
        assertThat(true, is(root.isLeaf()));

        root.setFirstChild(new TreeNode<String>("descendant"));
        assertThat(false, is(root.isLeaf()));
        assertThat(true, is(root.getFirstChild().isLeaf()));
    }

    @Test
    public void testIsLastSiebling() throws Exception {
      //  assertThat(true, is(false));
    }

    @Test
    public void testSetSiblings_DoNotAllowToAddSiblingToRoot() {
       // assertThat(true, is(false));
    }

}