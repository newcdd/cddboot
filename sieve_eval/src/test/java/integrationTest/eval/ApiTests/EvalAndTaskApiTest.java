package integrationTest.eval.ApiTests;

/**
 * Created by pandyc01 on 01/06/2016.
 */
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResult;
import com.botw.aml.sieve.eval.domain.model.eval.ScoreCalculator;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.party.KycProfile;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataImpl;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.port.adapter.rest.Application;
import com.botw.aml.sieve.eval.util.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.CddEvaluationTask;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.boot.test.SpringApplicationConfiguration;
import java.net.URL;
import java.util.*;
import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class EvalAndTaskApiTest {

    private RestTemplate restTemplate = new TestRestTemplate();
    private static final double DELTA = 1e-15;

    @Value("${local.server.port}")
    private int port;

    @Value("${integrationtest.hostname}")
    private String hostname;

 /*   @Value("${riskmodel.filePath}")
    private static String riskModelFile;*/

    private URL base;
    private MockMvc mvc;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    @Autowired
    WebApplicationContext context;

    @Before
    public void setUp() throws Exception {

        this.base = new URL(hostname + port + "/");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    @Test
    public void testTaskAndEvalApi() throws Exception{

        Set<String> kycfactids = new HashSet<String>();
        ScoreCalculator scoreCalculator = new ScoreCalculator();

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        CddEvaluationTask ceval = new CddEvaluationTask();
        HttpEntity<String> httpEntity = new HttpEntity<String>(OBJECT_MAPPER.writeValueAsString(ceval), requestHeaders);
        CddEvaluationTask res = restTemplate.postForObject(base.toString()+"task/", httpEntity, CddEvaluationTask.class, Collections.EMPTY_MAP);
        KycEvaluationResult apiResponse = restTemplate.getForObject(base.toString()+"evaluation/"+res.getEvalId(), KycEvaluationResult.class);

<<<<<<< Updated upstream:sieve_eval/src/test/java/integrationTest/eval/ApiTests/EvalAndTaskApiTest.java
        List<TreeNode<RiskFactorDataImpl>> ctn = new ArrayList<TreeNode<RiskFactorDataImpl>>();
        ctn = KycProfile.convertRiskModel("D:\\rm.xls");
        RiskModel riskModel = new RiskModel("name", "version");
=======
        List<TreeNode<RiskFactorDataSet>> ctn = new ArrayList<TreeNode<RiskFactorDataSet>>();
        ctn = KycProfile.convertRiskModel("C:\\Users\\manij01\\Others\\rm.xls");
        RiskModel riskModel = new RiskModel("name", "version",1,"Country","USA",5);
>>>>>>> Stashed changes:sieve_eval/src/test/java/integrationTest/eval/EvalAndTaskApiTest.java
        riskModel.setRiskFactorTree(ctn);
        kycfactids.add("1.0");
        kycfactids.add("3.0");
        kycfactids.add("4.0");
        kycfactids.add("5.0");
        kycfactids.add("6.0");
        kycfactids.add("8.0");

        EvaluationResult evaluationResult = scoreCalculator.evaluate(riskModel, kycfactids);

        assertNotNull(apiResponse);
<<<<<<< Updated upstream:sieve_eval/src/test/java/integrationTest/eval/ApiTests/EvalAndTaskApiTest.java
        //assertEquals(apiResponse.getTotalScore(),evaluationResult.getScore(),DELTA);
=======
       // assertEquals(apiResponse.getTotalScore(),evaluationResult.getScore(),DELTA);
>>>>>>> Stashed changes:sieve_eval/src/test/java/integrationTest/eval/EvalAndTaskApiTest.java

    }
}


