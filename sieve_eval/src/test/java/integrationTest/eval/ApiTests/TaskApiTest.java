package integrationTest.eval.ApiTests;

/**
 * Created by pandyc01 on 01/06/2016.
 */
import com.botw.aml.sieve.eval.port.adapter.rest.Application;
import com.botw.aml.sieve.eval.util.TestCaseParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.CddEvaluationTask;
import io.swagger.model.CddProfile;
import io.swagger.model.FactGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.boot.test.SpringApplicationConfiguration;
import java.net.URL;
import java.util.*;
import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class TaskApiTest {

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();
    private static final double DELTA = 1e-15;
    private static Log log = LogFactory.getLog(TaskApiTest.class);

    @Value("${local.server.port}")
    private int port;

    @Value("${integrationtest.hostname}")
    private String hostname;

   /* @Value("${riskmodel.filePath}")
    private String riskModelFile;*/

    private URL base;
    private MockMvc mvc;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static Log log = LogFactory.getLog(TaskApiTest.class);


        @Autowired
        WebApplicationContext context;

    @Before
    public void setUp() throws Exception {

        this.base = new URL(hostname + port + "/");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }
/*
    @Test
    public void taskAPITest() throws Exception {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        CddEvaluationTask ceval = new CddEvaluationTask();
        HttpEntity<String> httpEntity = new HttpEntity<String>(OBJECT_MAPPER.writeValueAsString(ceval), requestHeaders); //Creating http entity object with request body and headers
        CddEvaluationTask res = restTemplate.postForObject(base.toString()+"task/", httpEntity, CddEvaluationTask.class, Collections.EMPTY_MAP);
        assertTrue(res.getEvalId().contains("-"));

    }*/


    @Test
    public void taskAPITest() throws Exception {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
       // CddEvaluationTask ceval = new CddEvaluationTask();
        CddProfile cddProfile = setInputObject();
        log.info("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        System.out.println("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        HttpEntity<String> httpEntity = new HttpEntity<String>(OBJECT_MAPPER.writeValueAsString(cddProfile), requestHeaders); //Creating http entity object with request body and headers
        CddEvaluationTask res = restTemplate.postForObject(base.toString()+"task/", httpEntity, CddEvaluationTask.class, Collections.EMPTY_MAP);
        log.info("resval"+res);
        System.out.println("resval"+res);
       // assertTrue(res.getEvalId().contains("-"));
    }

    public CddProfile setInputObject()
    {
        FactGroup factGroup=new FactGroup();
        List<String>factKeysList=new ArrayList();
        CddProfile cddProfile=new CddProfile();

        //Consider GroupKey as RMID
        factGroup.setGroupKey("785674");

        //Set the factKeysList
        factKeysList.add("1.0");
        factKeysList.add("3.0");
        factKeysList.add("4.0");
        factKeysList.add("5.0");
        factKeysList.add("6.0");
        factKeysList.add("8.0");

        factGroup.setFactKeys(factKeysList);
        cddProfile.setPartyFacts(factGroup);

        return cddProfile;

    }

}


