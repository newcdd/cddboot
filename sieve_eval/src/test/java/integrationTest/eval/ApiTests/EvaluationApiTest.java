package integrationTest.eval.ApiTests;

/**
 * Created by pandyc01 on 01/06/2016.
 */
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.port.adapter.rest.Application;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.boot.test.SpringApplicationConfiguration;
import java.net.URL;

import static org.junit.Assert.*;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration

@IntegrationTest("server.port=0")
public class EvaluationApiTest {

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();
    private static final double DELTA = 1e-15;

    @Value("${local.server.port}")
    private int port;

    @Value("${integrationtest.hostname}")
    private String hostname;



    private URL base;
    private MockMvc mvc;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    @Autowired
    WebApplicationContext context;

    @Before
    public void setUp() throws Exception {

        this.base = new URL(hostname + port + "/");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    @Test
    public void testEvalResultApi() throws Exception{
        String evalId = "id001";
        KycEvaluationResult apiResponse = restTemplate.getForObject(base.toString()+"evaluation/"+evalId, KycEvaluationResult.class);
        assertNotNull(apiResponse);
        assertEquals(apiResponse.getTotalScore(), 100.0,DELTA);

    }
}


