package integrationTest.eval.Parsers;

/**
 * Created by pandyc01 on 01/06/2016.
 */

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.port.adapter.rest.Application;
import com.botw.aml.sieve.eval.util.TestCaseParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.CddEvaluationTask;
import io.swagger.model.CddProfile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.net.URL;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class ScoreModelParserTest {

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();
    private static final double DELTA = 1e-15;

    @Value("${local.server.port}")
    private int port;

    @Value("${integrationtest.hostname}")
    private String hostname;



    private URL base;
    private MockMvc mvc;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static Log log = LogFactory.getLog(ScoreModelParserTest.class);


    @Autowired
    WebApplicationContext context;

    @Before
    public void setUp() throws Exception {

        this.base = new URL(hostname + port + "/");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    @Test
    public void parseExcelTest() throws Exception {

        TestCaseParser testCaseParser = new TestCaseParser();
        String[][] parseData = testCaseParser.parseExcel("D:\\kyc_testdata.xls");

        assertNotNull(parseData);


    }


}


