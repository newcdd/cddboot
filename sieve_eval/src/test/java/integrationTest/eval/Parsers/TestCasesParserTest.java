package integrationTest.eval.Parsers;

/**
 * Created by pandyc01 on 01/06/2016.
 */

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResult;
import com.botw.aml.sieve.eval.domain.model.eval.ScoreCalculator;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.party.KycProfile;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataImpl;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.port.adapter.rest.Application;
import com.botw.aml.sieve.eval.util.TestCaseParser;
import com.botw.aml.sieve.eval.util.TreeNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.model.CddEvaluationTask;
import io.swagger.model.CddProfile;
import io.swagger.model.FactGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import java.net.URL;
import java.util.*;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port=0")
public class TestCasesParserTest {

    //Test RestTemplate to invoke the APIs.
    private RestTemplate restTemplate = new TestRestTemplate();
    private static final double DELTA = 1e-15;

    @Value("${local.server.port}")
    private int port;

    @Value("${integrationtest.hostname}")
    private String hostname;



    private URL base;
    private MockMvc mvc;
    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static Log log = LogFactory.getLog(TestCasesParserTest.class);


    @Autowired
    WebApplicationContext context;

    @Before
    public void setUp() throws Exception {

        this.base = new URL(hostname + port + "/");
        mvc = MockMvcBuilders.webAppContextSetup(context).build();

    }

    @Test
    public void taskAPIParserTest() throws Exception {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        // CddEvaluationTask ceval = new CddEvaluationTask();
        TestCaseParser testCaseParser = new TestCaseParser();
        String[][] parseData = testCaseParser.parseExcel("D:\\kyc_testdata.xls");
        List<CddProfile> cddProfileList = testCaseParser.getCddProfileList(parseData);
        CddProfile cddProfile = cddProfileList.get(0);
        log.info("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        System.out.println("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        HttpEntity<String> httpEntity = new HttpEntity<String>(OBJECT_MAPPER.writeValueAsString(cddProfile), requestHeaders); //Creating http entity object with request body and headers
        CddEvaluationTask res = restTemplate.postForObject(base.toString()+"task/", httpEntity, CddEvaluationTask.class, Collections.EMPTY_MAP);
        KycEvaluationResult apiResponse = restTemplate.getForObject(base.toString()+"eval/"+res.getEvalId(), KycEvaluationResult.class);

        assertNotNull(apiResponse);

    }


    @Test
    public void parseExcelTest() throws Exception {

        TestCaseParser testCaseParser = new TestCaseParser();
        String[][] parseData = testCaseParser.parseExcel("D:\\kyc_testdata.xls");

        assertNotNull(parseData);


    }

    @Test
    public void getCddProfileListTest() throws Exception {

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
        // CddEvaluationTask ceval = new CddEvaluationTask();
        TestCaseParser testCaseParser = new TestCaseParser();
        String[][] parseData = testCaseParser.parseExcel("D:\\kyc_testdata.xls");
        List<CddProfile> cddProfileList = testCaseParser.getCddProfileList(parseData);
        CddProfile cddProfile = cddProfileList.get(0);
        log.info("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        System.out.println("setInputObject"+cddProfile.getPartyFacts().getGroupKey());
        HttpEntity<String> httpEntity = new HttpEntity<String>(OBJECT_MAPPER.writeValueAsString(cddProfile), requestHeaders); //Creating http entity object with request body and headers
        CddEvaluationTask res = restTemplate.postForObject(base.toString()+"task/", httpEntity, CddEvaluationTask.class, Collections.EMPTY_MAP);
        KycEvaluationResult apiResponse = restTemplate.getForObject(base.toString()+"eval/"+res.getEvalId(), KycEvaluationResult.class);

        assertNotNull(apiResponse);

        assertTrue(!cddProfileList.isEmpty());

    }


}


