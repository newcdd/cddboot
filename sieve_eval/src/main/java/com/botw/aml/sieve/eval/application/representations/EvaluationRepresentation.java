package com.botw.aml.sieve.eval.application.representations;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

/**
 * Created by krasnd52 on 12/8/15.
 */
public final class EvaluationRepresentation {
    private String riskRating;

    protected EvaluationRepresentation() {
        super();
    }

    public EvaluationRepresentation(KycEvaluationResult evaluationResult) {
        this();
        this.initializeFrom(evaluationResult);
    }

    private void initializeFrom(KycEvaluationResult anEvaluationResult) {
        this.setRiskRating(anEvaluationResult.getRating().toString());
    }

    public String getRiskRating() {
        return riskRating;
    }

    private void setRiskRating(String aRiskRating) {
        this.riskRating = aRiskRating;
    }
}
