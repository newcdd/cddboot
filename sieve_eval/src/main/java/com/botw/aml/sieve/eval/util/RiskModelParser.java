package com.botw.aml.sieve.eval.util;

import com.botw.aml.sieve.eval.domain.model.riskModel.Fact;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataImpl;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by pandyc01 on 01/14/2016.
 */
public class RiskModelParser {

    private Map<String, TreeNode<RiskFactorDataImpl>> tree;
    private List<RiskModelRow> rmList;


    public RiskModelParser (String riskModelSource){
        this.tree = new HashMap<String, TreeNode<RiskFactorDataImpl>>();
        rmList = parseExcel(riskModelSource);
        List<TreeNode<RiskFactorDataImpl>> ctn = parse(rmList);
    }

    /*  public static void main(String[] args){

          List<RiskModelRow> rmList = new ArrayList<RiskModelRow>();
          RiskModelParser riskModelParser = new RiskModelParser("D:\\newrm2.xls");
          rmList = riskModelParser.parseExcel("D:\\newrm2.xls");
          List<TreeNode<RiskFactorDataImpl>> ctn = riskModelParser.parse(rmList);

      }
  */
    List<RiskModelRow> findChildQuestionsInList(String parentQuestion, List<RiskModelRow> rmList) {

        List<RiskModelRow> children = new ArrayList<RiskModelRow>();
        for (RiskModelRow row : rmList) {
            if (row.question == parentQuestion) {
                children.add(row);
            }
        }
        return children;
    }

    public void setNextSiblingNode(RiskModelRow sibling, RiskModelRow nextSibling) {

        System.out.println(" Setting Next Sibling ......");

        TreeNode node = tree.get(sibling.getId());//tree.get(sibling1.getId());
        TreeNode nextNode =  tree.get(nextSibling.getId());// tree.get(sibling2.getId());
        if (node == null) {

            Fact fact = new Fact(sibling.getFactid(),"",sibling.getQuestion(),sibling.getAnswer(),sibling.getScore());
            RiskFactorDataImpl rfSibling1  = new RiskFactorDataImpl(fact,sibling.getScore());
            node = new TreeNode(rfSibling1);
            tree.put(sibling.getId(), node);
        }

        if (nextNode == null ) {
            Fact fact = new Fact(nextSibling.getFactid(),"",nextSibling.getQuestion(),nextSibling.getAnswer(),nextSibling.getScore());
            RiskFactorDataImpl rfSibling2  = new RiskFactorDataImpl(fact,nextSibling.getScore());
            nextNode = new TreeNode(rfSibling2);
            tree.put(nextSibling.getId(), nextNode);
        }

        node.setNextSibling(nextNode);

    }

    public void setFirstChildNode(RiskModelRow child, RiskModelRow parent) {

        TreeNode childNode =  tree.get(child.getId());// tree.get(child.getId());
        System.out.println(" Setting First Child ......Child Fact ID ::"+child.getFactid() + "::" +child.getQuestion() +"::" +child.getAnswer());

        if (childNode == null) {

            Fact fact = new Fact(child.getFactid(),"",child.getQuestion(),child.getAnswer(),child.getScore());
            RiskFactorDataImpl rfChild  = new RiskFactorDataImpl(fact,child.getScore());
            childNode = new TreeNode(rfChild);
            tree.put(child.getId(), childNode);
        }

        TreeNode parentNode = tree.get(parent.getFactid());//tree.get(parent.getId());

        if (parentNode == null ) {
            Fact fact = new Fact(parent.getFactid(),"",parent.getQuestion(),parent.getAnswer(),parent.getScore());
            RiskFactorDataImpl rfParent  = new RiskFactorDataImpl(fact,parent.getScore());
            parentNode = new TreeNode(rfParent);
            tree.put(parent.getId(), parentNode);
        }

        parentNode.setFirstChild(childNode);
    }

    public List<TreeNode<RiskFactorDataImpl>> parse(List<RiskModelRow> rmList) {

        System.out.println("Start Parsing ...");
        List<TreeNode<RiskFactorDataImpl>> ctn = new ArrayList<TreeNode<RiskFactorDataImpl>>();


        for (RiskModelRow rmRow : rmList) {
            System.out.println("Current Question ::" + rmRow.question +"  Current Answer ::" + rmRow.getAnswer());
            System.out.println("Next Question ::" + rmRow.nextQuestion);

            if (!rmRow.nextQuestion.equals("NA")) {

                List<RiskModelRow> children = findChildQuestionsInList(rmRow.nextQuestion, rmList);
                if (children.size() > 0) {//process children in a linked way

                    RiskModelRow lastProcessedChild = null;
                    lastProcessedChild = children.get(0);
                    String firstchild = "something";
                    System.out.println("Children size ::" + children.size());

                    for (RiskModelRow childRow : children) {
                        System.out.println("lastProcessedChild"+lastProcessedChild.question + "::" + lastProcessedChild.getAnswer());
                        //process siblings
                        if (firstchild != null) {   //lastProcessedChild == null
                            //this is our first iteration
                            lastProcessedChild = childRow;
                            this.setFirstChildNode(childRow, rmRow);
                            firstchild = null;
                        } else {
                            this.setNextSiblingNode(lastProcessedChild, childRow);
                        }
                    }
                }else{
                    // if no child
                    // TreeNode tn = tree.get(rmRow.getFactid());
                    System.out.print("NO Child Row ID :" + rmRow.getFactid()  );

                }
            }else{   // if null next question
                  /*  System.out.println("Null Next question getId ::");
                    System.out.println("Null Next question getId ::"+ rmRow.getId());
                    Fact fact = new Fact(rmRow.getFactid(),"",rmRow.getQuestion(),rmRow.getAnswer(),rmRow.getScore());
                    RiskFactorDataImpl rfds  = new RiskFactorDataImpl(fact,0.0);
                   // rfds.setFact(fact);
                    TreeNode node = new TreeNode(rfds);// tree.get(rmRow.getId());
                    node.setFirstChild(null);
                    tree.put(rmRow.getFactid(),node);
                    */
            }
            System.out.println("--------------------------------------------------------------------------------------------------- ");
        }


        for (Map.Entry<String, TreeNode<RiskFactorDataImpl>> entry : tree.entrySet()) {
            System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());

            ctn.add(entry.getValue());
        }

        System.out.println("CTN Size : " + ctn.size());


        for (int i = 0; i < ctn.size(); i++) {
            //  System.out.println(ctn.get(i));
            System.out.println(ctn.get(i).getData().fact().getId()+"---"+ctn.get(i).getData().fact().getQuestionCode()+"---"+ctn.get(i).getData().fact().getAnswerCode() + "---"+ctn.get(i).getFirstChild()+"---" + ctn.get(i).getFirstChild());
        }
        return ctn;
    }

    public List<RiskModelRow> parseExcel(String path){
        List<RiskModelRow> rmList = new ArrayList<RiskModelRow>();


        try {
            File inputWorkbook = new File(path);
            Workbook w;
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(0);    // Get the first sheet

            for (int row = 1; row < 10; row++) {   //sheet.getRows()
                RiskModelRow riskModelRow = new RiskModelRow();
                for (int col = 0; col < 5; col++) {   //sheet.getColumns()
                    Cell cell = sheet.getCell(col, row);
                    CellType type = cell.getType();
                    System.out.print(cell.getContents() + " | ");

                    if (col == 0) {
                        if (cell.getContents() == null || cell.getContents() == "") {
                            break;
                        }
                        riskModelRow.setFactid(Double.parseDouble(cell.getContents()));
                    }
                    if (col == 1) {
                        riskModelRow.setQuestion(cell.getContents());
                    }
                    if (col == 2) {
                        riskModelRow.setAnswer(cell.getContents());
                    }
                    if (col == 3) {
                        riskModelRow.setScore(Double.parseDouble(cell.getContents()));
                    }
                    if (col == 4) {
                        riskModelRow.setNextQuestion(cell.getContents());
                    }

                }

                rmList.add(riskModelRow);
                System.out.println("::::"+rmList.size());
            }
        }catch (BiffException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        return rmList;

    }
}
