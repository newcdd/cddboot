package com.botw.aml.sieve.eval.domain.model.task;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;

import java.net.URL;
import java.util.Date;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class CddEvaluationTask {
  
  private String evalId = null;
  private Date pingAfter = null;
  private URL taskURL = null;

  
  /**
   * Unique identifier of the evaluation task
   **/
  @ApiModelProperty(value = "Unique identifier of the evaluation task")
  @JsonProperty("eval_id")
  public String getEvalId() {
    return evalId;
  }
  public void setEvalId(String evalId) {
    this.evalId = evalId;
  }

  
  /**
   * An estimated time of completion of the CDD evaluation.
   **/
  @ApiModelProperty(value = "An estimated time of completion of the CDD evaluation.")
  @JsonProperty("ping_after")
  public Date getPingAfter() {
    return pingAfter;
  }
  public void setPingAfter(Date pingAfter) {
    this.pingAfter = pingAfter;
  }


  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddEvaluationTask {\n");
    
    sb.append("  evalId: ").append(evalId).append("\n");
    sb.append("  pingAfter: ").append(pingAfter).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
