package com.botw.aml.sieve.eval.application.representations;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

/**
 * Created by krasnd52 on 12/8/15.
 */
public final class EvaluationWithDetailsRepresentation {
    private String riskRating;
    private double totalScore;

    protected EvaluationWithDetailsRepresentation() {
        super();
    }

    public EvaluationWithDetailsRepresentation(KycEvaluationResult anEvaluationResult) {
        this();
        this.initializeFrom(anEvaluationResult);
    }

    public double getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(double totalScore) {
        this.totalScore = totalScore;
    }

    private void initializeFrom(KycEvaluationResult anEvaluationResult) {
        this.setTotalScore(anEvaluationResult.getTotalScore());
        EvaluationRepresentation ratingOnlyRepresentation = new EvaluationRepresentation(anEvaluationResult);
        this.setRiskRating(ratingOnlyRepresentation.getRiskRating());


    }

    public String getRiskRating() {
        return riskRating;
    }

    private void setRiskRating(String riskRating) {
        this.riskRating = riskRating;
    }

}
