package com.botw.aml.sieve.eval.domain.model.account;

import com.botw.aml.sieve.eval.domain.model.infrastructure.Application;
import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Created by krasnd52 on 12/7/15.
 */
public final class AccountKey implements Serializable {
    private String accountNumber;
    private Application bookOfRecord;

    public AccountKey(String anAccountNumber, Application aBookOfRecord) {

        this.setAccountNumber(anAccountNumber);
        this.setBookOfRecord(aBookOfRecord);
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    private void setAccountNumber(String anAccountNumber) {
        this.accountNumber = anAccountNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccountKey that = (AccountKey) o;
        return Objects.equal(getAccountNumber(), that.getAccountNumber()) &&
                Objects.equal(getBookOfRecord(), that.getBookOfRecord());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getAccountNumber(), getBookOfRecord());
    }

    public Application getBookOfRecord() {
        return bookOfRecord;
    }

    private void setBookOfRecord(Application aBookOfRecord) {
        this.bookOfRecord = aBookOfRecord;

    }
}
