package com.botw.aml.sieve.eval.domain.model.party;

import com.botw.aml.sieve.eval.domain.model.account.KyaEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResult;
import com.botw.aml.sieve.eval.domain.model.eval.RatingCalculator;
import com.botw.aml.sieve.eval.domain.model.eval.ScoreAggregator;
import com.botw.aml.sieve.eval.domain.model.eval.ScoreCalculator;
import com.botw.aml.sieve.eval.domain.model.riskModel.Fact;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorData;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataSet;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.port.adapter.rest.EventHolderBean;
import com.botw.aml.sieve.eval.util.TreeNode;
import com.botw.aml.sieve.eval.util.TreeNode2;

import java.io.*;
import java.util.*;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by krasnd52 on 12/3/15.
 */
//@Component
public final class KycProfile {

    private final PartyContext partyContext;
    private Collection<KyaProfile> kyaProfiles;
    private Set<String> kycFactIds;

   /* @Autowired
    private ApplicationContext ctx;*/


    public KycProfile(
            PartyContext aPartyContext,
            Set<String> kycFactIds,
            Collection<KyaProfile> kyaProfiles
    ) {
        this.partyContext = aPartyContext;
        this.setKyaProfiles(kyaProfiles);
        this.setKycFactIds(kycFactIds);
    }

    public PartyContext getPartyContext() {
        return partyContext;
    }

    public Collection<KyaProfile> getKyaProfiles() {
        return kyaProfiles;
    }

    private void setKyaProfiles(Collection<KyaProfile> kyaProfiles) {
        this.kyaProfiles = kyaProfiles;
    }

    public Set<String> getKycFactIds() {
        return kycFactIds;
    }

    public void setKycFactIds(Set<String> kycFactIds) {
        this.kycFactIds = kycFactIds;
    }

    /**
     * Method to evaluate teh
     * KycFactIds and calculate the
     * score
     * @param anEvaluationId
     * @param kycFactIds
     * @return
     */
    public KycEvaluationResult evaluate(String anEvaluationId,Set<String>kycFactIds,RiskModel riskModel)
    {
        KycEvaluationResult kycEvaluationResult= new KycEvaluationResult(anEvaluationId);

        EventHolderBean eventBean=null;

        System.out.println("**********RiskModel from context*******"+riskModel);

        ScoreCalculator scoreCalculator = new ScoreCalculator();
        System.out.println("Riskmodel ::" + riskModel.getQuestion());
        EvaluationResult evaluationResult = scoreCalculator.evaluate(riskModel, kycFactIds);

        System.out.println("Evaluation Result score ::"+ evaluationResult.getScore()+ "RiskModel Size"+ riskModel.getRiskFactorTree().size());
        kycEvaluationResult.setKycResult(evaluationResult);

        return kycEvaluationResult ;
    }
    /**
     * Constructing Collection of tree nodes
     * from risk model
     * @param path
     * @return
     */
    public static List<TreeNode<RiskFactorDataSet>> convertRiskModel(String path){

        List<TreeNode<RiskFactorDataSet>> ctn = new ArrayList<TreeNode<RiskFactorDataSet>>();
        List<RiskFactorDataSet> rfd = new ArrayList<RiskFactorDataSet>();

        try {
            File inputWorkbook = new File(path);
            Workbook w;
            w = Workbook.getWorkbook(inputWorkbook);
            Sheet sheet = w.getSheet(0);    // Get the first sheet
            TreeNode<RiskFactorDataSet> firstChild = null;
            TreeNode<RiskFactorDataSet> nextSibling = null;
            TreeNode<RiskFactorDataSet> pnode = null;
            TreeNode<RiskFactorDataSet> snode = null;
            String val = "";
            String parentTN = null;
            int index =0;
            Double id = 0.0;
            String question = null;
            String ans = null;
            Double score = 0.0;
            String outerloop = "not null";

            for (int j = 1; j < sheet.getRows(); j++) {
                for (int i = 0; i < sheet.getColumns(); i++) {
                    Cell cell = sheet.getCell(i, j);
                    CellType type = cell.getType();
                    System.out.print(cell.getContents() +" | ");

                    if(i==0){
                        if (cell.getContents() == null || cell.getContents() == ""){outerloop=null;break;}
                        id = Double.parseDouble(cell.getContents());
                    }
                    if(i==1){
                        question = cell.getContents();
                    }
                    if(i==2){
                        ans = cell.getContents();
                    }
                    if(i==3){
                        score = Double.parseDouble(cell.getContents());

                    }

                    if(i == 4){
                        parentTN =cell.getContents();
                    }
                }

                if(outerloop == null) {break;}

                Fact fact = new Fact(id,"",question,ans,score);  //Fact2 fact1 = new Fact2(1.0,"first", "first node" , "first node data",0.0);
                System.out.println("Setting factid " + id);
                RiskFactorDataSet rf = new RiskFactorDataSet() {
                    @Override
                    public Fact fact() {
                        return null;
                    }

                    @Override
                    public double riskWeight() {
                        return 0;
                    }
                };
                rf.setRiskWeight(score);
                rf.setFact(fact);
                rfd.add(rf);
                ctn.add(new TreeNode<RiskFactorDataSet>(rfd.get(j-1),firstChild,nextSibling));
                System.out.print("Last index of CTN :"+ (ctn.size()));
                if(parentTN.equals("null")){     // parent = null
                    if(ctn.isEmpty() || ctn.size() == 1) {
                        //ctn.get(j).setFirstChild(null);
                        //ctn.get(j).setNextSibling(null);
                    }else {
                       /* int ind = 0;
                        for (ind =0 ; ind < ctn.size() ;ind ++){

                            if(ctn.get(ind).getNextSibling() == null){
                                ctn.get(ind).setNextSibling(ctn.get(j-1));
                                break;
                            }

                        }*/

                        pnode = ctn.get(0);
                        snode = ctn.get(0);
                        while (snode != null) {
                            snode = pnode.getNextSibling();
                            System.out.println("1" + snode);
                            if (snode == null) {
                                System.out.println("2");
                                index = ctn.indexOf(pnode);
                                System.out.println("3");
                                ctn.get(index).setNextSibling(ctn.get(j-1));
                                System.out.println("4");
                                break;
                            }
                            System.out.println("5");
                            pnode = pnode.getNextSibling();
                            //ctn.get(index).setNextSibling(ctn.get(j-1));  //set Nth next sibling of the null Parent
                            //  ctn.get(index).setNextSibling(ctn.get(j));  //set Nth next sibling of the null Parent
                        }
                        // ctn.get(j).setFirstChild(null);

                    }
                }

                if(!parentTN.equals("null")){                 // parent = n
                    System.out.println("Index of CTN ::"+ ctn.indexOf(ctn.get(Integer.parseInt(parentTN))));
                    System.out.println("ParentTN when not null ::"+ Integer.parseInt(parentTN) + (ctn.get(Integer.parseInt(parentTN))).getFirstChild());
                    if((ctn.get(Integer.parseInt(parentTN)-1)).getFirstChild() == null) {
                        ctn.get(Integer.parseInt(parentTN)-1).setFirstChild(ctn.get(j-1));   //set firstChild of Parent Node
                    }else{
                        if(ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().getNextSibling() == null) {
                            ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().setNextSibling(ctn.get(j-1));   //set first sibling of firtChild of same Parent
                        }else{
                            // pnode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().getNextSibling();
                            // snode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().getNextSibling();
                            pnode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild();
                            snode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild();
                            while(snode != null){
                                snode = pnode.getNextSibling();
                                if(snode == null){
                                    index = ctn.indexOf(pnode);
                                    ctn.get(index).setNextSibling(ctn.get(j-1));
                                    break;
                                }
                                pnode = pnode.getNextSibling();
                                // ctn.get(index).setNextSibling(ctn.get(j-1));  //set Nth next sibling of the same Parent
                            }
                        }
                    }
                }
                System.out.println("j = "+ j);
            }
        } catch (BiffException e) {
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        return ctn;
    }
}
