package com.botw.aml.sieve.eval.port.adapter.rest;

import com.botw.aml.sieve.eval.domain.model.riskModel.Fact;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataSet;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.util.TreeNode;
import jxl.Cell;
import jxl.CellType;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by BOTW on 01/20/2016.
 */
    @Component
    //TODO This should basically be changed to the RISKModel
    public class EventHolderBean
    {
        @Autowired
        private ResourceLoader resourceLoader;

        private RiskModel riskModel;


        public RiskModel getEventFired()
        {
            File rmFile=null;
            RiskModel riskModel=null;
            Resource resource = resourceLoader.getResource("classpath:rm.xls");
            try
            {
                rmFile = resource.getFile();
                System.out.println(rmFile.getName());

                riskModel = new RiskModel("name", "version",1,"Country","USA",5);

                List<TreeNode<RiskFactorDataSet>> ctn = new ArrayList<TreeNode<RiskFactorDataSet>>();
                //System.out.println("riskModelFile"+riskModelFile);
                ctn = convertRiskModel(rmFile);
                riskModel.setRiskFactorTree(ctn);

                System.out.println("FactId" +"||" + "Question" + " || " +"Ans" +" || "+" Score " + " ||" + "First Child " + " || " + "Next Sibling");
                for (int x = 0 ; x < ctn.size() ; x++){
                    String fchild = "NA" ;
                    String nsibling = "NA" ;
                    if(ctn.get(x).getFirstChild() != null) {
                        fchild = ctn.get(x).getFirstChild().getData().getFact().getLongDesc();
                    }
                    if(ctn.get(x).getNextSibling() != null) {
                        nsibling = ctn.get(x).getNextSibling().getData().getFact().getLongDesc();
                    }

                    System.out.println(ctn.get(x).getData().getFact().getId() +" || " +
                            ctn.get(x).getData().getFact().getShortDesc() +" || " +
                            ctn.get(x).getData().getFact().getLongDesc() +" || " +
                            ctn.get(x).getData().getFact().getRiskWeight() +" || " +
                            fchild+ " || " +
                            nsibling );
                }

            } catch (IOException e)
            {
                e.printStackTrace();
            }
            return riskModel;
        }

        public void setEventFired(RiskModel riskModel) {
            this.riskModel = riskModel;
        }

        private List<TreeNode<RiskFactorDataSet>> convertRiskModel(File riskModel)
        {

            List<TreeNode<RiskFactorDataSet>> ctn = new ArrayList<TreeNode<RiskFactorDataSet>>();
            List<RiskFactorDataSet> rfd = new ArrayList<RiskFactorDataSet>();
            File inputWorkbook=null;
            if(null!=riskModel)
            {
                try {
                    inputWorkbook = riskModel;
                    Workbook w;
                    w = Workbook.getWorkbook(inputWorkbook);
                    Sheet sheet = w.getSheet(0);    // Get the first sheet
                    TreeNode<RiskFactorDataSet> firstChild = null;
                    TreeNode<RiskFactorDataSet> nextSibling = null;
                    TreeNode<RiskFactorDataSet> pnode = null;
                    TreeNode<RiskFactorDataSet> snode = null;
                    String val = "";
                    String parentTN = null;
                    int index = 0;
                    Double id = 0.0;
                    String question = null;
                    String ans = null;
                    Double score = 0.0;
                    String outerloop = "not null";

                    for (int j = 1; j < sheet.getRows(); j++) {
                        for (int i = 0; i < sheet.getColumns(); i++) {
                            Cell cell = sheet.getCell(i, j);
                            CellType type = cell.getType();
                            System.out.print(cell.getContents() + " | ");

                            if (i == 0) {
                                if (cell.getContents() == null || cell.getContents() == "") {
                                    outerloop = null;
                                    break;
                                }
                                id = Double.parseDouble(cell.getContents());
                            }
                            if (i == 1) {
                                question = cell.getContents();
                            }
                            if (i == 2) {
                                ans = cell.getContents();
                            }
                            if (i == 3) {
                                score = Double.parseDouble(cell.getContents());

                            }

                            if (i == 4) {
                                parentTN = cell.getContents();
                            }
                        }

                        if (outerloop == null) {
                            break;
                        }

                        Fact fact = new Fact(id, "", question, ans, score);  //Fact2 fact1 = new Fact2(1.0,"first", "first node" , "first node data",0.0);
                        System.out.println("Setting factid " + id);
                        RiskFactorDataSet rf = new RiskFactorDataSet() {
                            @Override
                            public Fact fact() {
                                return null;
                            }

                            @Override
                            public double riskWeight() {
                                return 0;
                            }
                        };
                        rf.setRiskWeight(score);
                        rf.setFact(fact);
                        rfd.add(rf);
                        ctn.add(new TreeNode<RiskFactorDataSet>(rfd.get(j - 1), firstChild, nextSibling));
                        System.out.print("Last index of CTN :" + (ctn.size()));
                        if (parentTN.equals("null")) {     // parent = null
                            if (ctn.isEmpty() || ctn.size() == 1)
                            {
                                //ctn.get(j).setFirstChild(null);
                                //ctn.get(j).setNextSibling(null);
                            } else
                            {
                            /*int ind = 0;
                            for (ind =0 ; ind < ctn.size() ;ind ++){

                                if(ctn.get(ind).getNextSibling() == null){
                                    ctn.get(ind).setNextSibling(ctn.get(j-1));
                                    break;
                                }

                            }*/

                                pnode = ctn.get(0);
                                snode = ctn.get(0);
                                while (snode != null) {
                                    snode = pnode.getNextSibling();
                                    System.out.println("1" + snode);
                                    if (snode == null) {
                                        System.out.println("2");
                                        index = ctn.indexOf(pnode);
                                        System.out.println("3");
                                        ctn.get(index).setNextSibling(ctn.get(j - 1));
                                        System.out.println("4");
                                        break;
                                    }
                                    System.out.println("5");
                                    pnode = pnode.getNextSibling();
                                    //ctn.get(index).setNextSibling(ctn.get(j-1));  //set Nth next sibling of the null Parent
                                    //  ctn.get(index).setNextSibling(ctn.get(j));  //set Nth next sibling of the null Parent
                                }
                                // ctn.get(j).setFirstChild(null);

                            }
                        }

                        if (!parentTN.equals("null")) {                 // parent = n
                            System.out.println("Index of CTN ::" + ctn.indexOf(ctn.get(Integer.parseInt(parentTN))));
                            System.out.println("ParentTN when not null ::" + Integer.parseInt(parentTN) + (ctn.get(Integer.parseInt(parentTN))).getFirstChild());
                            if ((ctn.get(Integer.parseInt(parentTN) - 1)).getFirstChild() == null) {
                                ctn.get(Integer.parseInt(parentTN) - 1).setFirstChild(ctn.get(j - 1));   //set firstChild of Parent Node
                            } else {
                                if (ctn.get(Integer.parseInt(parentTN) - 1).getFirstChild().getNextSibling() == null) {
                                    ctn.get(Integer.parseInt(parentTN) - 1).getFirstChild().setNextSibling(ctn.get(j - 1));   //set first sibling of firtChild of same Parent
                                } else {
                                    // pnode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().getNextSibling();
                                    // snode = ctn.get(Integer.parseInt(parentTN)-1).getFirstChild().getNextSibling();
                                    pnode = ctn.get(Integer.parseInt(parentTN) - 1).getFirstChild();
                                    snode = ctn.get(Integer.parseInt(parentTN) - 1).getFirstChild();
                                    while (snode != null) {
                                        snode = pnode.getNextSibling();
                                        if (snode == null) {
                                            index = ctn.indexOf(pnode);
                                            ctn.get(index).setNextSibling(ctn.get(j - 1));
                                            break;
                                        }
                                        pnode = pnode.getNextSibling();
                                        // ctn.get(index).setNextSibling(ctn.get(j-1));  //set Nth next sibling of the same Parent
                                    }
                                }
                            }
                        }
                        System.out.println("j = " + j);
                    }
                } catch (BiffException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return ctn;
        }
    }
