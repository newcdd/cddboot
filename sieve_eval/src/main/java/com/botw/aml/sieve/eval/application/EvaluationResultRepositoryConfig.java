package com.botw.aml.sieve.eval.application;

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResultRepository;
import com.botw.aml.sieve.eval.port.adapter.persistence.H2DbEvaluationResultRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pandyc01 on 01/10/2016.
 */
@Configuration
public class EvaluationResultRepositoryConfig {

   /* @Bean
    public OracleDbEvaluationResultRepository getOracleEvaluationResultRepo(){
        return new OracleDbEvaluationResultRepository();
    }

    @Bean
    public H2DbEvaluationResultRespository getH2EvaluationResultRepo(){
        return new H2DbEvaluationResultRespository();
    }*/

    @Bean
    public EvaluationResultRepository getEvaluationResultRepo(){
        return new H2DbEvaluationResultRepository();

        // return new OracleDbEvaluationResultRepository();
    }

}

