package com.botw.aml.sieve.eval.port.adapter.rest;

import com.botw.aml.sieve.eval.application.ApplicationServiceRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pandyc01 on 01/10/2016.
 */
@Configuration
public class ApplicationServiceRegistryConfig {

    @Bean
    ApplicationServiceRegistry getApplicationServiceRegistry(){
        return new ApplicationServiceRegistry();
    }
}