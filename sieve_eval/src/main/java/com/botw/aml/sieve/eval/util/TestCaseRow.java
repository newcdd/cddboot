package com.botw.aml.sieve.eval.util;

/**
 * Created by pandyc01 on 01/14/2016.
 */
public class TestCaseRow {

    Double factid ;
    String riskfactor;
    String question;
    String answer;
    String xpathtag;

    String getId(){
        return question + answer;
    }

    public String getQuestion() {
        return question;
    }

    public Double getFactid() {
        return factid;
    }

    public void setFactid(Double factid) {
        this.factid = factid;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getRiskfactor() {
        return riskfactor;
    }

    public void setRiskfactor(String riskfactor) {
        this.riskfactor = riskfactor;
    }

    public String getXpathtag() {
        return xpathtag;
    }

    public void setXpathtag(String xpathtag) {
        this.xpathtag = xpathtag;
    }
}
