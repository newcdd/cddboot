package com.botw.aml.sieve.eval.application;

import com.botw.aml.sieve.eval.application.commands.EvaluateCommand;
import com.botw.aml.sieve.eval.application.commands.InitiateEvaluationCommand;
import com.botw.aml.sieve.eval.domain.model.eval.Evaluation;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResultRepository;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationService;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.task.CddEvaluationTask;
import com.botw.aml.sieve.eval.port.adapter.persistence.H2DbEvaluationResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.UUID;

/**
 * Created by krasnd52 on 12/8/15.
 */
//@Transactional
@Component
public class EvaluationApplicationService {

    private EvaluationResultRepository evaluationResultRepository;

    @Autowired
    private EvaluationService evaluationService;

    @Autowired
    public static ApplicationContext ctx;

    @Autowired
    private H2DbEvaluationResultRepository DbEvaluationResultRepository;


    private EvaluationService evaluationService() {
        return evaluationService;
    }

    /**
     * Schedules an evaluation of a KYC profile by the Sieve engine. This method is non-blocking and returns immediately
     * (i.e. before the evaluation has finished). The returned Evaluation object allows you to query
     * the progress of the transfer, add listeners for progress events, and wait for the upload to complete.
     * <p/>
     * If resources are available, the upload will begin immediately, otherwise it will be scheduled and started as soon
     * as resources become available.
     *
     * @param aCommand
     * @return A new Evaluation object which can be used to check state of the evaluation, listen for progress
     * notifications, and otherwise manage the upload.
     */
    public Evaluation initiateEvaluation(InitiateEvaluationCommand aCommand) {
        Evaluation evaluation =
                evaluationService
                        .submit(aCommand.getKycProfile().getPartyContext().getAmlPartyId(),
                                aCommand.getKycProfile().getPartyContext().getPartyKeys(),
                                aCommand.getKycProfile().getKycFactIds(),
                                aCommand.getKycProfile().getKyaProfiles());
        return evaluation;
    }

    /**
     * The synchronous method for evaluating a KYC profile.
     *
     * @param aCommand
     */
    public void evaluate(EvaluateCommand aCommand) {

        KycEvaluationResult evaluationResult =
                this.evaluationService().evaluate(
                        aCommand.getEvaluationId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getAmlPartyId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getPartyKeys(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKycFactIds(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKyaProfiles());

        this.evaluationResultRepository().add(evaluationResult);

    }

    //@Transactional(readOnly = true)
    public KycEvaluationResult evaluationResultOf(String anEvaluationId) {

        KycEvaluationResult evaluationResult = DbEvaluationResultRepository.evalutionWithId(anEvaluationId);


        return evaluationResult;
    }

    public void evaluate(KycEvaluationResult kycEvaluationResult) {

        DbEvaluationResultRepository.add(kycEvaluationResult);
        
    }

    public Collection<KycEvaluationResult> evalHistoryWithDetailsOf(String aGlobalPartyId) {

        return this.evaluationResultRepository()
                .allEvaluationResultsFor(
                        AmlPartyId.existingEvaluationId(aGlobalPartyId)
                );
    }

    public Collection<KycEvaluationResult> evalHistoryOf(String aGlobalPartyId) {

        return this.evaluationResultRepository()
                .allEvaluationResultsFor(
                        AmlPartyId.existingEvaluationId(aGlobalPartyId)
                );
    }

    private EvaluationResultRepository evaluationResultRepository() {
        return this.evaluationResultRepository;
    }


    public CddEvaluationTask generateTaskId() {

       /* KycEvaluationResult evaluationResult =
                this.evaluationService().evaluate(
                        aCommand.getEvaluationId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getAmlPartyId(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getPartyContext().getPartyKeys(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKycFactIds(),
                        aCommand.getInitiateEvaluationCommand().getKycProfile().getKyaProfiles());

        this.evaluationResultRepository().add(evaluationResult);
        */
        CddEvaluationTask cddEvaluationTask = new CddEvaluationTask();
        String taskId = UUID.randomUUID().toString();

        cddEvaluationTask.setEvalId(taskId);
        cddEvaluationTask.setPingAfter(new Date());
       /* try {
            cddEvaluationTask.setTaskURL(new URL("http://localhost:8080/eval/"));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }*/

        return cddEvaluationTask;
    }
}
