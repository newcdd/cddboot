package com.botw.aml.sieve.eval.port.adapter.persistence;

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResultRepository;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.util.Collection;

/**
 * Created by krasnd52 on 12/14/15.
 */
public class OracleDbEvaluationResultRespository implements EvaluationResultRepository {
    @Override
    public void add(KycEvaluationResult anEvaluationResult) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Collection<KycEvaluationResult> allEvaluationResultsFor(AmlPartyId anAmlPartyId) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public KycEvaluationResult evalutionWithId(String anEvaluationId) {
        throw new UnsupportedOperationException("not implemented");
    }
}
