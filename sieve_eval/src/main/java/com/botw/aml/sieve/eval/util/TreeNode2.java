package com.botw.aml.sieve.eval.util;

import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorData;

/**
 * Created by krasnd52 on 12/4/15.
 */
public class TreeNode2<RiskFactorData2> {
    private RiskFactorData2 data;
    private TreeNode2<RiskFactorData2> firstChild;
    private TreeNode2<RiskFactorData2> nextSibling;

    public TreeNode2(RiskFactorData2 data ,  TreeNode2<RiskFactorData2> firstChild,  TreeNode2<RiskFactorData2> nextSibling) {
        this.data = data;
        firstChild = firstChild;
        nextSibling = nextSibling;
    }

    public RiskFactorData2 getData() {
        return data;
    }

    public TreeNode2<RiskFactorData2> setData(RiskFactorData2 data) {
        this.data = data;
        return this;
    }

    public TreeNode2<RiskFactorData2> getFirstChild() {
        return firstChild;
    }

    public TreeNode2<RiskFactorData2> setFirstChild(TreeNode2<RiskFactorData2> firstChild) {
        this.firstChild = firstChild;
        return this;
    }

    public TreeNode2<RiskFactorData2> getNextSibling() {
        return nextSibling;
    }

    public TreeNode2<RiskFactorData2> setNextSibling(TreeNode2<RiskFactorData2> nextSibling) {
        this.nextSibling = nextSibling;
        return this;
    }

    public boolean isLeaf() {
        return null == getFirstChild();
    }

    public boolean isLastSibling() {
        return null == getNextSibling();
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", firstChild=" + firstChild +
                ", nextSibling=" + nextSibling +
                '}';
    }
}
