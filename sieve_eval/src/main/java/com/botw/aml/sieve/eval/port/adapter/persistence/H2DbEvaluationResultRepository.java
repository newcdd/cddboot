package com.botw.aml.sieve.eval.port.adapter.persistence;

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResultRepository;
import com.botw.aml.sieve.eval.domain.model.infrastructure.Application;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.*;
import java.util.*;

import com.botw.aml.sieve.eval.port.adapter.rest.EvaluationResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;


/**
 * Created by krasnd52 on 12/14/15.
 */
@Repository
public class H2DbEvaluationResultRepository implements EvaluationResultRepository {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public void add(KycEvaluationResult anEvaluationResult) {

        try {
            System.out.println("************jdbcTemplate************88888"+jdbcTemplate.getDataSource());
            System.out.println("************EvaluationID************88888"+anEvaluationResult.getEvaluationId());
            jdbcTemplate.update("INSERT INTO CDD_EVAL(EVAL_ID,SCORE,kycEvalResult) VALUES(?,?,?)",anEvaluationResult.getEvaluationId(), anEvaluationResult.getTotalScore(),anEvaluationResult);
        }catch (Exception e){
            e.printStackTrace();
        }
           System.out.println("Row inserted to H2");
    }

    @Override
    public Collection<KycEvaluationResult> allEvaluationResultsFor(AmlPartyId anAmlPartyId) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public KycEvaluationResult evalutionWithId(String anEvaluationId)  {

        KycEvaluationResult kycEvaluationResult = new KycEvaluationResult();

        try {
            kycEvaluationResult = jdbcTemplate.queryForObject("SELECT SCORE FROM CDD_EVAL WHERE EVAL_ID=?", kycEvaluationResultRowMapper, anEvaluationId);

       }catch (Exception e){
            e.printStackTrace();
        }

         return kycEvaluationResult;
    }

    private RowMapper<KycEvaluationResult> kycEvaluationResultRowMapper = new RowMapper<KycEvaluationResult>() {
        public KycEvaluationResult mapRow(ResultSet rs, int rowNum) throws SQLException {
            KycEvaluationResult kycEvaluationResult = new KycEvaluationResult();
            kycEvaluationResult.setTotalScore(Double.parseDouble(rs.getString("SCORE")));

            System.out.println("SCORES::::: "+kycEvaluationResult.getTotalScore());
            return kycEvaluationResult;
        }
    };
}
