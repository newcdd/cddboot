package com.botw.aml.sieve.eval.util;

import io.swagger.model.CddProfile;
import io.swagger.model.FactGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by BOTW on 01/14/2016.
 */
public class ServiceUtil
{

    private static Log log = LogFactory.getLog(ServiceUtil.class);

    /**
     * Method to fetch the kycFactIds
     * from CddProfile object     *
     * @param cddProfile
     * @return
     */
    public Set populateInputVal(CddProfile cddProfile)
    {
        log.info("Entering populateInputVal"+cddProfile);

        Set<String> kycFactIds = new HashSet<String>();

        FactGroup factGroup = cddProfile.getPartyFacts();

        Iterator iterator = factGroup.getFactKeys().iterator();
        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
            kycFactIds.add(iterator.next().toString());
        }

        log.info("Exiting populateInputVal");
        return kycFactIds;
    }
}
