package com.botw.aml.sieve.eval.domain.model.riskModel;

/**
 * Created by pandyc01 on 01/14/2016.
 */
public class ScoreModel {

    private Double factid ;
    private String question;
    private String answer;
    private Double score;


    public String getId(){
        return question + answer;
    }

    public String getQuestion() {
        return question;
    }

    public Double getFactid() {
        return factid;
    }

    public void setFactid(Double factid) {
        this.factid = factid;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }
}
