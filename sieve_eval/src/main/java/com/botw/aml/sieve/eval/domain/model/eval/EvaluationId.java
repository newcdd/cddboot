package com.botw.aml.sieve.eval.domain.model.eval;

//   Copyright 2012,2013 Vaughn Vernon
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.


import com.botw.aml.sieve.common.domain.model.AbstractId;

import java.util.UUID;


public final class EvaluationId extends AbstractId {

    private static final long serialVersionUID = 1L;

    protected EvaluationId(String anId) {
        super(anId);
    }

    protected EvaluationId() {
        super();
    }

    public static EvaluationId existingEvaluationId(String anId) {
        EvaluationId evaluationId = new EvaluationId(anId);

        return evaluationId;
    }

    public static EvaluationId newEvaluationId() {
        EvaluationId evaluationId =
                new EvaluationId(UUID.randomUUID().toString().toLowerCase());

        return evaluationId;
    }



    @Override
    protected int hashOddValue() {
        return 3773;
    }

    @Override
    protected int hashPrimeValue() {
        return 43;
    }
}