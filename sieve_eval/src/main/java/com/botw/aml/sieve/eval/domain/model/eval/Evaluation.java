package com.botw.aml.sieve.eval.domain.model.eval;

/**
 * Created by krasnd52 on 11/20/15.
 * <p/>
 * Represents an asynchronous CDD/EDD evaluation of the bank's relationship with a party. Use this class to check an
 * evaluation's progress, add listeners for progress events, check the state of an evaluation, or wait for the
 * evaluation to complete.
 * <p/>
 * See KycEvaluationManager for more information about creating evaluation.
 */
public interface Evaluation {
    /**
     * Returns a unique identifier of the evaluation task.
     *
     * @return The UUID of the evaluation task
     */
    EvaluationId getEvaluationId();

    /**
     * Returns progress information about this evaluation.
     *
     * @return The progress information about this evaluation.
     */
    EvaluationProgress getProgress();

    /**
     * @return The current state of this evaluation.
     */
    EvaluationState getState();

    /**
     * @return Whether or not the evaluation is finished
     */
    boolean isDone();

    /**
     * Enumeration of the possible evaluation states.
     */
    enum EvaluationState {
        /**
         * The evaluation is waiting for resources to execute and has not started yet.
         */
        Waiting,
        /**
         * The evaluation is actively uploading or downloading and hasn't finished yet.
         */
        InProgress,

        /**
         * The evaluation completed successfully.
         */
        Completed,

        /**
         * The evaluation was canceled and did not complete successfully.
         */
        Canceled,

        /**
         * The evaluation failed.
         */
        Failed
    }
}
