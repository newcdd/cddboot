package com.botw.aml.sieve.eval.domain.model.party;

import com.botw.aml.sieve.eval.domain.model.account.KyaEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResult;
import com.botw.aml.sieve.eval.domain.model.riskModel.Rating;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Created by krasnd52 on 12/13/15.
 */
public final class KycEvaluationResult implements Serializable {
    private String evaluationId;
    private EvaluationResult kycResult;
    private Collection<KyaEvaluationResult> kyaResults;
    private double totalScore;
    private Rating rating;
    private Date evaluationDate;

    public KycEvaluationResult(int id ,String evalId ,Double totalScore) {
        this.setEvaluationId(evalId);
        this.setTotalScore(totalScore);
    }

    public KycEvaluationResult(String anEvaluationId) {
        this.setEvaluationId(anEvaluationId);
        this.kyaResults = new ArrayList<KyaEvaluationResult>();
    }

    public KycEvaluationResult() {
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    private KycEvaluationResult setEvaluationId(String anEvaluationId) {
        this.evaluationId = anEvaluationId;
        return this;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

    public KycEvaluationResult setEvaluationDate(Date evaluationDate) {
        this.evaluationDate = evaluationDate;
        return this;
    }

    public Rating getRating() {
        return rating;
    }

    public KycEvaluationResult setRating(Rating rating) {
        this.rating = rating;
        return this;
    }

    public double getTotalScore() {
        return totalScore;
    }

    public KycEvaluationResult setTotalScore(double totalScore) {
        this.totalScore = totalScore;
        return this;
    }

    public Collection<KyaEvaluationResult> getKyaResults() {
        return kyaResults;
    }

    public KycEvaluationResult addKyaResult(KyaEvaluationResult aKyaResult) {
        this.kyaResults.add(aKyaResult);
        return this;
    }

    public EvaluationResult getKycResult() {
        return kycResult;
    }

    public KycEvaluationResult setKycResult(EvaluationResult kycResult) {
        this.kycResult = kycResult;
        return this;
    }

}