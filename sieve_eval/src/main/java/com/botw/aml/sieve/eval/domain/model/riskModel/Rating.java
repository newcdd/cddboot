package com.botw.aml.sieve.eval.domain.model.riskModel;

/**
 * Created by krasnd52 on 12/6/15.
 */
public enum Rating {
    UNDEFINED, LOW, MEDIUM, HIGH
}

