package com.botw.aml.sieve.eval.domain.model.infrastructure;

import com.google.common.base.Objects;

/**
 * Created by krasnd52 on 12/7/15.
 */
public final class Application {
    String code;
    String name;

    public Application(String aCode, String aName) {
        this.setCode(aCode);
        this.setName(aName);
    }

    public String getCode() {

        return code;
    }

    private void setCode(String aCode) {
        this.code = aCode;
    }

    public String getName() {
        return name;
    }

    private void setName(String aName) {
        this.name = aName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Application that = (Application) o;
        return Objects.equal(getCode(), that.getCode()) &&
                Objects.equal(getName(), that.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getCode(), getName());
    }
}
