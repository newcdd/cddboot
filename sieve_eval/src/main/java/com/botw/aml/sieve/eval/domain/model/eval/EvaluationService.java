package com.botw.aml.sieve.eval.domain.model.eval;

import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import com.botw.aml.sieve.eval.domain.model.party.*;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.port.adapter.rest.EventHolderBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Set;

/**
 * Created by krasnd52 on 12/8/15.
 */
@Component
public class EvaluationService {

    /*@Autowired
    private KycProfile kycProfile;*/

    @Autowired
    private ApplicationContext ctx;


    public Evaluation submit(final AmlPartyId anAmlPartyId,  // coming here
                             final Set<PartyKey> partyKeys,
                             final Set<String> kycFactIds,
                             final Collection<KyaProfile> kyaProfiles) {
        return new SpringScheduledEvaluation(
                EvaluationId.newEvaluationId(),
                new EvaluationProgress(),
                new KycProfile(
                        PartyContext.createPartyContext(
                                anAmlPartyId,
                                partyKeys),
                        kycFactIds,
                        kyaProfiles
                ));
    }

    public KycEvaluationResult evaluate(
            final String anEvaluationId,
            final AmlPartyId anAmlPartyId,
            final Set<PartyKey> partyKeys,
            final Set<String> kycFactIds,
            final Collection<KyaProfile> kyaProfiles
    ) {
        System.out.println("*******EvaluationSErvice evaluate");
        KycProfile kycProfile = new KycProfile(
                PartyContext.createPartyContext(
                        anAmlPartyId,
                        partyKeys),
                kycFactIds,
                kyaProfiles);
        System.out.println("Entering before getting context"+ctx);
        EventHolderBean eventHolderBean=ctx.getBean(EventHolderBean.class);
        System.out.println("Entering EvaluationSErvice"+eventHolderBean);
        return kycProfile.evaluate(anEvaluationId,kycFactIds,eventHolderBean.getEventFired());
        //return kycProfile.newEvaluate();
    }
}
