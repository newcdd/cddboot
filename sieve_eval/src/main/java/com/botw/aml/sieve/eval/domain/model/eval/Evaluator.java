package com.botw.aml.sieve.eval.domain.model.eval;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;

import java.util.Set;

/**
 * Created by krasnd52 on 12/6/15.
 */
public interface Evaluator {
    void evaluate(
            final RiskModel riskModel,
            final Set<Double> factIds,
            KycEvaluationResult kycEvaluationResult);

}