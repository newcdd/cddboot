package com.botw.aml.sieve.eval.application.commands;

import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycProfile;
import com.botw.aml.sieve.eval.domain.model.party.PartyContext;
import com.botw.aml.sieve.eval.domain.model.party.PartyKey;

import java.util.Collection;
import java.util.Set;

/**
 * Created by krasnd52 on 12/8/15.
 */
public class InitiateEvaluationCommand {
    private KycProfile kycProfile;

    protected InitiateEvaluationCommand() {
        super();
    }

    public InitiateEvaluationCommand(String aGlobalPartyId,
                                     Set<PartyKey> partyKeys,
                                     Collection<KyaProfile> kyaProfiles,
                                     Set<String> kycFacts) {
        this();
        setKycProfile(
                new KycProfile(
                        PartyContext.createPartyContext(
                                AmlPartyId.existingEvaluationId(aGlobalPartyId),
                                partyKeys),
                        kycFacts,
                        kyaProfiles));
    }

    public KycProfile getKycProfile() {
        return kycProfile;
    }

    private void setKycProfile(KycProfile aKycProfile) {
        this.kycProfile = aKycProfile;
    }
}
