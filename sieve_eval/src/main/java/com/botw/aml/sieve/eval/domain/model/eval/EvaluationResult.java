package com.botw.aml.sieve.eval.domain.model.eval;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by krasnd52 on 12/4/15.
 * <p/>
 * Encapsulate an evaluation result of a set of facts. May be used for KYA, KYC, or, if preferred, its
 * use may be slightly refactored so it can be used for a subtree (i.e. what the business calls a 'risk factor' today
 */
public final class EvaluationResult implements Serializable {
    private final Set<Double> usedInput;



    // private final Set<Double> skippedInput;
    private final Set<String> skippedInput;
    private final Set<Double> missingInput;
    private double score;


    public EvaluationResult(Set<String> factIds) {
        this.score = 0;
        this.skippedInput = factIds;
        this.usedInput = new HashSet<Double>();
        this.missingInput = new HashSet<Double>();
    }

    public Set<Double> getUsedInput() {
        return usedInput;
    }

    public Set<Double> getMissingInput() {
        return missingInput;
    }

    public double getScore() {
        return score;
    }

    public void increaseScore(double score) {
        this.score += score;
    }

    public void addMissingFact(double aMissingFactId) {
        this.missingInput.add(aMissingFactId);
    }

    public void addFoundFact(double aFoundFactId) {
        this.usedInput.add(aFoundFactId);
    }

    public void removeFromSkippedInput(double aFoundFactId) {
        this.skippedInput.remove(aFoundFactId);
    }

    public Set<String> getSkippedInput() { return skippedInput; }

}
