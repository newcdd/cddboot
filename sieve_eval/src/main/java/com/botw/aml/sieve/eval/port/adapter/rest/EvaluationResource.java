package com.botw.aml.sieve.eval.port.adapter.rest;

import com.botw.aml.sieve.common.serializer.ObjectSerializer;
import com.botw.aml.sieve.eval.application.EvaluationApplicationService;
import com.botw.aml.sieve.eval.application.representations.EvaluationWithDetailsRepresentation;
import com.botw.aml.sieve.eval.domain.model.party.*;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by krasnd52 on 12/8/15.
 * An Evaluation adapter for the RESTful HTTP port provided by the JAX-RS implementation
 * <p/>
 * It delegates to the application service EvaluationService.
 */
@RestController
public class EvaluationResource extends AbstractResource {


    @Autowired
    private EvaluationApplicationService evaluationApplicationService;

    public EvaluationResource() {
        super();
    }

    @RequestMapping(value= "/evaluation/{evalId}",method=RequestMethod.GET,produces = "application/json")
    public KycEvaluationResult getEvaluation(@PathVariable("evalId") String evalId,@Context Request aRequest){  //@RequestParam(value = "evaluationId", required = false) String anEvaluationId

        KycEvaluationResult evaluationResult = evaluationApplicationService.evaluationResultOf(evalId); //anEvaluationId
        System.out.print("Evaluation Score :::::::::::::::::::::"+ evaluationResult.getTotalScore());
        if (evaluationResult == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        return evaluationResult;
    }

    private Response evaluationResponse(Request aRequest, KycEvaluationResult anEvaluationResult) {

        Response response;

        EntityTag eTag = this.evaluationETag(anEvaluationResult);

        Response.ResponseBuilder conditionalBuilder = aRequest.evaluatePreconditions(eTag);

        if (conditionalBuilder != null) {
            response =
                    conditionalBuilder
                            .cacheControl(this.cacheControlFor(3600))
                            .tag(eTag)
                            .build();
        } else {
            String representation =
                    ObjectSerializer
                            .instance()
                            .serialize(new EvaluationWithDetailsRepresentation(anEvaluationResult));

            response =
                    Response
                            .ok(representation)
                            .cacheControl(this.cacheControlFor(3600))
                            .tag(eTag)
                            .build();
        }

        return response;
    }

}
