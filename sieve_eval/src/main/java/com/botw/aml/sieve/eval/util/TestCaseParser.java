package com.botw.aml.sieve.eval.util;

import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataImpl;
import io.swagger.model.CddProfile;
import io.swagger.model.FactGroup;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by pandyc01 on 01/14/2016.
 */
public class TestCaseParser {

    Map<String, TreeNode<RiskFactorDataImpl>> tree = new HashMap<String, TreeNode<RiskFactorDataImpl>>();

    public TestCaseParser(){

        String[][] parseData = this.parseExcel("D:\\kyc_testdata.xls");
    }

   /* public static void main(String[] args){

        List<CddProfile> cddProfileList = new ArrayList<CddProfile>();
        TestCaseParser testCaseParser = new TestCaseParser();
        String[][] data = testCaseParser.parseExcel("D:\\kyc_testdata.xls");
        cddProfileList = testCaseParser.getCddProfileList(data);

        System.out.println(" Group Key :::"+cddProfileList.get(0).getPartyFacts().getGroupKey());
        System.out.println(" fact Keys :::"+cddProfileList.get(0).getPartyFacts().getFactKeys());
        System.out.println(" CddProfileList Size ::::"+cddProfileList.size());
        System.out.println(" Data ::::"+data[3][7]);
        System.out.println(" Data Sizw::::"+data.length + "::"+data[0].length);

        for(int row = 0; row < 74; row++)
        {
            for(int col = 0; col <74; col++)
            {
                System.out.print(data[row][col] + " |  ");
            }
            System.out.println();
        }
    }*/

    public List<CddProfile> getCddProfileList(String[][] data){

        List<CddProfile> cddProfileList = new ArrayList<CddProfile>();
        String key = null;
        String groupKey = null;

        for (int col = 7; col < data[0].length ; col++) { //data[0].length
            List<String> factkeys = new ArrayList<String>();
            FactGroup factGroup = new FactGroup();
            CddProfile cddProfile = new CddProfile();

            for (int row = 2; row < data.length; row++) { //data.length
                if(row==2){
                    groupKey = data[row][col];
                }else {
                    key = data[row][3] + data[row][col];
                    System.out.println("GroupKey" + groupKey);
                    System.out.println("Key" + key);
                    if(groupKey != null)
                        factkeys.add(key);

                    System.out.println("List Size ::" + factkeys.size());
                }
            }
            factGroup.setGroupKey(groupKey);
            factGroup.setFactKeys(factkeys);
            cddProfile.setAccountFacts(null);
            cddProfile.setPartyFacts(factGroup);
            if(factGroup.getGroupKey() != null && !factGroup.getGroupKey().isEmpty()) {
                cddProfileList.add(cddProfile);
            }
        }

        return cddProfileList;
    }

    public String[][] parseExcel(String path){

        String[][] data = null;
        try {

            FileInputStream file = new FileInputStream(new File(path));
            Workbook workbook =  WorkbookFactory.create(file);
            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();

            data = new String[sheet.getPhysicalNumberOfRows()][sheet.getRow(0).getPhysicalNumberOfCells()]; //sheet.getLastRowNum()

            int rowcount = 0;
            int cellcount = 0;
            while (rowIterator.hasNext())
            {
                cellcount = 0;
                Row row = rowIterator.next();

                Iterator<Cell> cellIterator = row.cellIterator();
                while (cellIterator.hasNext())
                {
                    Cell cell = cellIterator.next();
                    cell.setCellType(Cell.CELL_TYPE_STRING);

                    if (cell != null || cell.getCellType() != Cell.CELL_TYPE_BLANK) {
                        data[rowcount][cellcount] = cell.getStringCellValue();
                        cellcount++;
                    }
                }
                System.out.println("");
                rowcount++;
                System.out.println("Row Count :" + rowcount + " Cell count :" + cellcount);
            }
            file.close();

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return data;

    }
}
