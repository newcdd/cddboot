package com.botw.aml.sieve.eval.port.adapter.rest;

import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by BOTW on 01/20/2016.
 */
    @Component
    public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent>{

        private EventHolderBean eventHolderBean;

        @Autowired
        public void setEventHolderBean(EventHolderBean eventHolderBean) {
            this.eventHolderBean = eventHolderBean;
        }

        @Override
        public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
            System.out.println("*********Context Event Received***********");
             RiskModel rm = new RiskModel("name", "version",1,"Country","USA",5);
            eventHolderBean.setEventFired(rm);
        }


    }
