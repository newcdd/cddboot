package com.botw.aml.sieve.eval.port.adapter.rest; /**
 * Created by pandyc01 on 12/16/2015.
 */

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.botw.aml.sieve.eval")
public class Application {

    public static ApplicationContext context;

    public static void main(final String[] args)
    {
        //System.out.println("RiskModelValue"+riskModelFile);
        //context = SpringApplication.run(Application.class, args);
        context = SpringApplication.run(Application.class, args);
        EventHolderBean bean = context.getBean(EventHolderBean.class);
        System.out.println("************Risk Model FileName?? - " + bean.getEventFired());
    }
}
