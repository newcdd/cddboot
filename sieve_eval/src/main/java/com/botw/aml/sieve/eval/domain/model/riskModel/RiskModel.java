package com.botw.aml.sieve.eval.domain.model.riskModel;

import com.botw.aml.sieve.eval.util.TreeNode;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by krasnd52 on 12/3/15.
 */
public class RiskModel {
    public final String name;
    public final String version;

    private Collection<TreeNode<RiskFactorData>> riskFactorTrees;
    private Collection<TreeNode<RiskFactorDataImpl>> riskFactorTree;
    private Map<String,ScoreModel> scoreModel;

    public RiskModel(String name, String version ) {
        this.name = name;
        this.version = version;
    }

    //RiskModelParser parser = new RiskModelParser("D:\\newrm2.xls"); //TODO call the parser, populate the risk model

    public String getName() {
        return name;
    }  // TODO remove this

    public String getVersion() {
        return version;
    }

    public double getThreshold(Rating rating) {
        double threshold;
        switch (rating) {
            case LOW:
                threshold = 0; //TODO move thresholds into a config file / db
                break;
            case MEDIUM:
                threshold = 500; //TODO move thresholds into a config file / db
                break;
            case HIGH:
                threshold = 1000; //TODO move thresholds into a config file / db
                break;
            case UNDEFINED:
                threshold = Double.MIN_VALUE;
                break;
            default:
                throw new IllegalArgumentException("unsupported rating");
        }
        return threshold;
    }

    public String name() {
        return this.name;
    }

    public String version() {
        return this.version;
    }

    public Collection<TreeNode<RiskFactorData>> getRiskFactorTrees() {
        return riskFactorTrees;
    }

    public Collection<TreeNode<RiskFactorDataImpl>> getRiskFactorTree() {
        return riskFactorTree;
    }

    public void setRiskFactorTree(Collection<TreeNode<RiskFactorDataImpl>> ctn) {

        this.riskFactorTree = ctn;
    }

    public Map<String, ScoreModel> getScoreModel() {
        return scoreModel;
    }

    public void setScoreModel(Map<String, ScoreModel> scoreModel) {
        this.scoreModel = scoreModel;
    }


}
