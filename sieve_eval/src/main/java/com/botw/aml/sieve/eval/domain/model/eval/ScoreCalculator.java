package com.botw.aml.sieve.eval.domain.model.eval;

import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskFactorDataSet;
import com.botw.aml.sieve.eval.domain.model.riskModel.RiskModel;
import com.botw.aml.sieve.eval.util.TreeNode;
import com.botw.aml.sieve.eval.util.TreeNode2;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.Set;

/**
 * Created by krasnd52 on 12/4/15.
 */
public final class ScoreCalculator {

   /* public EvaluationResult evaluate(
            final RiskModel riskModel,
            final Set<Double> factIds)*/
    public EvaluationResult evaluate(
            final RiskModel riskModel,
            final Set<String> factIds)
    {
        EvaluationResult evaluationResult = new EvaluationResult(factIds);
        System.out.println("factid size in calc :::::"+ factIds.size() );
        System.out.println("factid size in calc :::::"+ riskModel.getRiskFactorTree().size() );

        Queue<TreeNode<RiskFactorDataSet>> queue = new ArrayDeque<TreeNode<RiskFactorDataSet>>();

        for (TreeNode<RiskFactorDataSet> root : riskModel.getRiskFactorTree()) {
            queue.add(root);
            while (!queue.isEmpty()) {
                TreeNode<RiskFactorDataSet> treeNode = queue.remove();
                System.out.println("Real Having factid :: "+ treeNode.getData().fact.getId() +" :::::"+ factIds.size()+"::"+ factIds );

                if (factIds.contains(treeNode.getData().fact.getId())) {
                    evaluationResult.increaseScore(treeNode.getData().riskWeight);
                    evaluationResult.addFoundFact(treeNode.getData().fact.getId());
                    evaluationResult.removeFromSkippedInput(treeNode.getData().fact.getId());

                    System.out.println("Having FactIDs ::::" + treeNode.getData().fact.getId());
                    System.out.println("Having Score ::::" +evaluationResult.getScore() );


                } else {
                    evaluationResult.addMissingFact(treeNode.getData().fact.getId());
                    System.out.println("Not Having FactIDs ::::" +treeNode.getData().fact.getId() );

                }
                //fact.accept(evaluationResult);
                if (!treeNode.isLeaf())
                    queue.add(treeNode.getFirstChild());
                if (!treeNode.isLastSibling())
                    queue.add(treeNode.getNextSibling());
            }
        }

        //TODO Verify with business users. If there were missing inputs, we could still provide the score / rank as is,
        // with the caveat of missing inputs. Obviously, the score and rank would be lower than it should be
        // If they don't want to provide a score / rank unless all necessary inputs were provided, the following statement
        // does it
        if (!evaluationResult.getMissingInput().isEmpty()) {
            //TODO mark eval result invalid, e.g. set score to -1 and add logic to score aggregator to correctly handle -1's
        }
        return evaluationResult;
    }
}
