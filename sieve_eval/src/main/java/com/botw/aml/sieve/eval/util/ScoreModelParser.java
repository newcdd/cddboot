package com.botw.aml.sieve.eval.util;

import com.botw.aml.sieve.eval.domain.model.riskModel.ScoreModel;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * Created by pandyc01 on 01/21/2016.
 */
public class ScoreModelParser {

    public ScoreModelParser(){

        Map<String,ScoreModel> scoreModelRowHashMap = this.parseExcel("D:\\scoremodel.xls");
    }

  /* public static void main(String[] args)
    {
        ScoreModelParser scoreModelParser = new ScoreModelParser();
        Map<String,ScoreModelRow> scoreModelRowHashMap = scoreModelParser.parseExcel("D:\\scoremodel.xls");
    }
*/

    public Map<String,ScoreModel> parseExcel(String path){

        Map<String,ScoreModel> scoreModelRowHashMap = new HashMap<String,ScoreModel>();
        try
        {
            FileInputStream file = new FileInputStream(new File(path));
            Workbook workbook =  WorkbookFactory.create(file);
            Sheet sheet = workbook.getSheetAt(0);
            int cellcount = 0;
            Iterator<Row> rowIterator = sheet.iterator();       //Iterate through each rows one by one
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator(); //For each row, iterate through all the columns
                ScoreModel scoreModel = new ScoreModel();
                cellcount = 0;
                if (row.getRowNum() == 0) {
                    continue;
                }

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    //cell.setCellType(Cell.CELL_TYPE_STRING);
                    cell.getNumericCellValue();
                    if(cellcount == 0) {
                        scoreModel.setFactid(cell.getNumericCellValue());
                    }
                    if (cellcount == 1) {
                        scoreModel.setQuestion(cell.getStringCellValue());
                    }
                    if (cellcount == 2) {
                        scoreModel.setAnswer(cell.getStringCellValue());
                    }
                    if (cellcount == 3) {
                        System.out.println(cell.getNumericCellValue());
                        scoreModel.setScore(cell.getNumericCellValue());
                    }

                    cellcount++;
                }

                scoreModelRowHashMap.put(scoreModel.getId(), scoreModel);
                file.close();

            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        for (Map.Entry<String, ScoreModel> entry : scoreModelRowHashMap.entrySet()) {
            System.out.println("Key : " + entry.getKey() + " Value : " + entry.getValue());
        }
        return scoreModelRowHashMap;
    }
}
