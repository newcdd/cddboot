package com.botw.aml.sieve.eval.domain.model.eval;

import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;

import java.util.Collection;

/**
 * Created by krasnd52 on 12/8/15.
 */
public interface EvaluationResultRepository {

    void add(KycEvaluationResult anEvaluationResult);

    Collection<KycEvaluationResult> allEvaluationResultsFor(AmlPartyId anAmlPartyId);

    KycEvaluationResult evalutionWithId(String anEvaluationId);

}
