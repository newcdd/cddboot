package com.botw.aml.sieve.eval.port.adapter.rest;

import com.botw.aml.sieve.eval.application.EvaluationApplicationService;
import com.botw.aml.sieve.eval.domain.model.account.KyaProfile;
import com.botw.aml.sieve.eval.domain.model.eval.Evaluation;
import com.botw.aml.sieve.eval.domain.model.eval.EvaluationService;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import com.botw.aml.sieve.eval.domain.model.party.PartyKey;
import com.botw.aml.sieve.eval.domain.model.task.CddEvaluationTask;
import com.botw.aml.sieve.eval.util.ServiceUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import io.swagger.api.NotFoundException;

import io.swagger.model.CddProfile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.*;
import java.util.*;

/*
@Api(value = "/task", description = "the task API")
@javax.annotation.Generated(value = "class io.io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-22T21:35:23.669Z")*/

@RestController
public class TasksApi extends AbstractResource {

  @Autowired
  private EvaluationApplicationService evaluationApplicationService;


    @Autowired
    private EvaluationService evaluationService;

    @Autowired
    private ApplicationContext ctx;

  public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private static Log log = LogFactory.getLog(TasksApi.class);

  /**
   * Method which gets the
   * Json object and calculates
   * the score
   * @param cddProfile
   * @return
   * @throws NotFoundException
     */
  @RequestMapping(value = "/task", produces = {"application/json"}, consumes = {"application/json"},
          method = RequestMethod.POST)
  public CddEvaluationTask tasksPost(@RequestBody CddProfile cddProfile)throws NotFoundException
  {
    Set<String> kycFactIds=null;
    Evaluation evaluation = null;
    KycEvaluationResult kycEvaluationResult = null;

    ServiceUtil serviceUtil = new ServiceUtil();
    CddEvaluationTask task = new CddEvaluationTask();
      EventHolderBean eventBean=null;
    System.out.println("cddProfilesys before"+cddProfile);

    log.info("CddProfile object input"+cddProfile);

    try
    {
      ObjectMapper objectMapper = new ObjectMapper();

     //
      AmlPartyId id = AmlPartyId.newEvaluationId();
      Set<PartyKey> partyKeys = new HashSet<PartyKey>();
      Collection<KyaProfile> kyaProfiles = new ArrayList<KyaProfile>();

      //Populate kycFactIds from CddProfileObject
      if (null != cddProfile)
       {
        log.info("CddProfile from Input" + cddProfile);
       // System.out.println("cddprofile value is now" + cddProfile);
        System.out.println("Application context in taskappi"+ctx);
        kycFactIds = serviceUtil.populateInputVal(cddProfile);

        //Fetch the EvaId and pingafter time
        evaluation = evaluationService.submit(id, partyKeys, kycFactIds, kyaProfiles);
        task.setPingAfter(new Date());
        task.setEvalId(evaluation.getEvaluationId().id());

        System.out.println("********taskID*******" + task.getEvalId());
         log.info("EvalID"+task.getEvalId());

        //Evaluate and calculate the score for KycFactIds
        kycEvaluationResult = evaluationService.evaluate(task.getEvalId(), id, partyKeys, kycFactIds, kyaProfiles);
           System.out.println("********Hi there*************"+kycEvaluationResult.getEvaluationId());
        evaluationApplicationService.evaluate(kycEvaluationResult);// Insert to H2 DB
      }
    }
    catch(Exception e)
    {

    }
    return task;
  }
}