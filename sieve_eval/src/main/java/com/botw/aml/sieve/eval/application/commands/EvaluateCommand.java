package com.botw.aml.sieve.eval.application.commands;

/**
 * Created by krasnd52 on 12/8/15.
 */
public class EvaluateCommand {
    private InitiateEvaluationCommand initiateEvaluationCommand;
    private String evaluationId;

    protected EvaluateCommand() {
        super();
    }

    public EvaluateCommand(
            String anEvaluationId,
            InitiateEvaluationCommand anInitiateEvaluationCommand) {
        this();
        setEvaluationId(anEvaluationId);
        setInitiateEvaluationCommand(anInitiateEvaluationCommand);
    }

    public InitiateEvaluationCommand getInitiateEvaluationCommand() {
        return initiateEvaluationCommand;
    }

    private void setInitiateEvaluationCommand(InitiateEvaluationCommand anInitiateEvaluationCommand) {
        this.initiateEvaluationCommand = anInitiateEvaluationCommand;
    }

    public String getEvaluationId() {
        return evaluationId;
    }

    private void setEvaluationId(String anEvaluationId) {
        this.evaluationId = anEvaluationId;
    }
}
