package com.botw.aml.sieve.eval.port.adapter.persistence;

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResultRepository;
import com.botw.aml.sieve.eval.domain.model.party.AmlPartyId;
import com.botw.aml.sieve.eval.domain.model.party.KycEvaluationResult;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * Created by krasnd52 on 12/14/15.
 */

@Component("ORA")
public class OracleDbEvaluationResultRepository implements EvaluationResultRepository {
    @Override
    public void add(KycEvaluationResult anEvaluationResult) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Collection<KycEvaluationResult> allEvaluationResultsFor(AmlPartyId anAmlPartyId) {
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public KycEvaluationResult evalutionWithId(String anEvaluationId) {
        System.out.println("ORACLE ::::::::::::::::::::::::::::");
        KycEvaluationResult kycEvaluationResult = new KycEvaluationResult("id001");
        return  kycEvaluationResult;
        //throw new UnsupportedOperationException("not implemented");
    }

}
