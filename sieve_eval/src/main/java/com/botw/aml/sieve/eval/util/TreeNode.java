package com.botw.aml.sieve.eval.util;

import java.util.UUID;

/**
 * Created by krasnd52 on 12/4/15.
 */
public class TreeNode<Data> {
    private Data data;
    private TreeNode<Data> firstChild;
    private TreeNode<Data> nextSibling;
    private String id;


    public TreeNode(Data data) {
        this.data = data;
        firstChild = null;
        nextSibling = null;
        this.id = UUID.randomUUID().toString().toLowerCase();
    }

    public TreeNode(Data data ,  TreeNode<Data> firstChild,  TreeNode<Data> nextSibling) {
        this.data = data;
        firstChild = firstChild;
        nextSibling = nextSibling;
    }

    public String getId(){
        return this.id;
    }


    public Data getData() {
        return data;
    }

    public TreeNode<Data> setData(Data data) {
        this.data = data;
        return this;
    }

    public TreeNode<Data> getFirstChild() {
        return firstChild;
    }

    public TreeNode<Data> setFirstChild(TreeNode<Data> firstChild) {
        this.firstChild = firstChild;
        return this;
    }

    public TreeNode<Data> getNextSibling() {
        return nextSibling;
    }

    public TreeNode<Data> setNextSibling(TreeNode<Data> nextSibling) {
        this.nextSibling = nextSibling;
        return this;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isLeaf() {
        return null == getFirstChild();
    }

    public boolean isLastSibling() {
        return null == getNextSibling();
    }

    @Override
    public String toString() {
        return "TreeNode{" +
                "data=" + data +
                ", firstChild=" + firstChild +
                ", nextSibling=" + nextSibling +
                '}';
    }
}
