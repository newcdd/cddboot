package com.botw.aml.sieve.eval.domain.model.eval;

/**
 * Created by krasnd52 on 11/30/15.
 * <p/>
 * Describes the progress of the evaluation. Eventually could be expanded to return granular information,
 * such as % of completed tasks.
 */
public final class EvaluationProgress {
    private static final long FIVE_SECONDS = 5;

    /**
     * Returns an estimated wait time (in seconds) until the evaluation is completed. Client uses it to determine how
     * long it should wait before polling for evaluation result.
     *
     * @return The
     */
    public long getWaitEstimateInSeconds() {
        //TODO Estimate a reasonable delay for the initial go live. Eventually a constant may be replaced by dynamic logic
        return FIVE_SECONDS;
    }

}
