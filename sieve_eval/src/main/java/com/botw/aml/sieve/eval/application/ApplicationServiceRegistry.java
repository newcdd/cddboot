package com.botw.aml.sieve.eval.application;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationServiceRegistry implements ApplicationContextAware {

    @Autowired
    private static ApplicationContext applicationContext;

    //@Autowired
    //private static EvaluationApplicationService evaluationApplicationService;

    public static EvaluationApplicationService evaluationApplicationService() {

        EvaluationApplicationService evaluationApplicationService = new EvaluationApplicationService();
        return  evaluationApplicationService;
        //NEEDs TO CHANGE "new" with proper bean config
       //return evaluationApplicationService; //(EvaluationApplicationService) applicationContext.getBean("evaluationApplicationService");

        //return (EvaluationApplicationService) applicationContext.getBean("evaluationApplicationService");

    }


    @Override
    public synchronized void setApplicationContext(
            ApplicationContext anApplicationContext)
            throws BeansException {

        if (ApplicationServiceRegistry.applicationContext == null) {
            ApplicationServiceRegistry.applicationContext = anApplicationContext;
        }
    }
}