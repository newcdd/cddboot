package com.botw.aml.sieve.eval.domain.model.account;

import com.botw.aml.sieve.eval.domain.model.eval.EvaluationResult;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by krasnd52 on 12/13/15.
 */
public final class KyaEvaluationResult implements Serializable {
    private final AccountContext accountContext;
    private final Date evaluationDate;
    private EvaluationResult evaluationResult;
    private double score;

    private KyaEvaluationResult(AccountContext anAccountContext) {
        this.accountContext = anAccountContext;
        this.evaluationDate = new Date();
    }

    public static KyaEvaluationResult createKyaEvaluationResult(AccountContext anAccountContext) {
        return new KyaEvaluationResult(anAccountContext);
    }

    public AccountContext getAccountContext() {
        return accountContext;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }


    public EvaluationResult getEvaluationResult() {
        return evaluationResult;
    }

    public KyaEvaluationResult setEvaluationResult(EvaluationResult aKyaResult) {
        this.evaluationResult = aKyaResult;
        return this;
    }

    public Date getEvaluationDate() {
        return evaluationDate;
    }

}