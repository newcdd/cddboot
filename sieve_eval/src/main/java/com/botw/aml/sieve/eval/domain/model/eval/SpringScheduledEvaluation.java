package com.botw.aml.sieve.eval.domain.model.eval;

import com.botw.aml.sieve.eval.domain.model.party.KycProfile;

/**
 * Created by krasnd52 on 12/14/15.
 */
//TODO This is a dummy implementation. Created for testing purposes only. Proper async implementation is required
public class SpringScheduledEvaluation implements Evaluation {
    private final EvaluationId evaluationId;
    private EvaluationProgress evaluationProgress;
    private EvaluationState evaluationState;

    public SpringScheduledEvaluation(
            EvaluationId anEvaluationId,
            EvaluationProgress anEvaluationProgress,
            KycProfile kycProfile) {
        this.evaluationId = anEvaluationId;
        this.evaluationProgress = anEvaluationProgress;
        this.evaluationState = EvaluationState.InProgress;
    }

    /**
     * Returns a unique identifier of the evaluation task.
     *
     * @return The UUID of the evaluation task
     */
    @Override
    public EvaluationId getEvaluationId() {
        return evaluationId;
    }

    /**
     * Returns progress information about this evaluation.
     *
     * @return The progress information about this evaluation.
     */
    @Override
    public EvaluationProgress getProgress() {
        return evaluationProgress;
    }

    /**
     * @return The current state of this evaluation.
     */
    @Override
    public EvaluationState getState() {
        return evaluationState;
    }

    /**
     * @return Whether or not the evaluation is finished
     */
    @Override
    public boolean isDone() {
        return false;
    }
}
