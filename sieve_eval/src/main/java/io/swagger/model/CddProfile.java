package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;

import java.util.ArrayList;
import java.util.List;


/**
 * A container object for the input required for computing a CDD score for a party. It includes data about the party and applicable accounts.
 **/
@ApiModel(description = "A container object for the input required for computing a CDD score for a party. It includes data about the party and applicable accounts.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class CddProfile  {
  
  private FactGroup partyFacts;
  private List<FactGroup> accountFacts;

  
  /**
   * Facts about the party being submitted for evaluation.
   **/
  @ApiModelProperty(value = "Facts about the party being submitted for evaluation.")
  @JsonProperty("party_facts")
  public FactGroup getPartyFacts() {
    return partyFacts;
  }
  public void setPartyFacts(FactGroup partyFacts) {
    this.partyFacts = partyFacts;
  }

  
  /**
   * An array of fact groups. Each fact groups represents a KYA profile.
   **/
  @ApiModelProperty(value = "An array of fact groups. Each fact groups represents a KYA profile.")
  @JsonProperty("account_facts")
  public List<FactGroup> getAccountFacts() {
    return accountFacts;
  }
  public void setAccountFacts(List<FactGroup> accountFacts) {
    this.accountFacts = accountFacts;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddProfile {\n");
    
    sb.append("  partyFacts: ").append(partyFacts).append("\n");
    sb.append("  accountFacts: ").append(accountFacts).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
