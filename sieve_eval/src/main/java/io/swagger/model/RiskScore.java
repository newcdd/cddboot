package io.swagger.model;

import java.util.*;

import io.swagger.annotations.*;
import com.fasterxml.jackson.annotation.JsonProperty;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class RiskScore  {
  
  private Double totalScore = null;
  private FactGroupScore kycResult = null;
  private List<FactGroupScore> kyaResults = new ArrayList<FactGroupScore>();

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("total_score")
  public Double getTotalScore() {
    return totalScore;
  }
  public void setTotalScore(Double totalScore) {
    this.totalScore = totalScore;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kyc_result")
  public FactGroupScore getKycResult() {
    return kycResult;
  }
  public void setKycResult(FactGroupScore kycResult) {
    this.kycResult = kycResult;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("kya_results")
  public List<FactGroupScore> getKyaResults() {
    return kyaResults;
  }
  public void setKyaResults(List<FactGroupScore> kyaResults) {
    this.kyaResults = kyaResults;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class RiskScore {\n");
    
    sb.append("  totalScore: ").append(totalScore).append("\n");
    sb.append("  kycResult: ").append(kycResult).append("\n");
    sb.append("  kyaResults: ").append(kyaResults).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
