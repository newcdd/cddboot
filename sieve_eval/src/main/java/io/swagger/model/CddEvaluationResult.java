package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;

import java.util.Date;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class CddEvaluationResult  {
  
  public enum RiskLevelEnum {
     unacceptable,  high,  medium,  low,  insufficient_info, 
  };
  private RiskLevelEnum riskLevel = null;
  private RiskScore riskScore = null;
  private CddProfileFindings findings = null;
  private RiskModel riskModel = null;
  private Date timestamp = null;

  
  /**
   * AML risk level of a party relationship for the bank.  * unacceptable - when encountered a hard stop(s)  * high * medium * low * insufficient _info - when one or more required facts are missing from the CDD profile
   **/
  @ApiModelProperty(value = "AML risk level of a party relationship for the bank.  * unacceptable - when encountered a hard stop(s)  * high * moderate * low * insufficient _info - when one or more required facts are missing from the CDD profile")
  @JsonProperty("risk_level")
  public RiskLevelEnum getRiskLevel() {
    return riskLevel;
  }
  public void setRiskLevel(RiskLevelEnum riskLevel) {
    this.riskLevel = riskLevel;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("risk_score")
  public RiskScore getRiskScore() {
    return riskScore;
  }
  public void setRiskScore(RiskScore riskScore) {
    this.riskScore = riskScore;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("findings")
  public CddProfileFindings getFindings() {
    return findings;
  }
  public void setFindings(CddProfileFindings findings) {
    this.findings = findings;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("risk_model")
  public RiskModel getRiskModel() {
    return riskModel;
  }
  public void setRiskModel(RiskModel riskModel) {
    this.riskModel = riskModel;
  }

  
  /**
   * Full date and time when the evaluation task completed.
   **/
  @ApiModelProperty(value = "Full date and time when the evaluation task completed.")
  @JsonProperty("timestamp")
  public Date getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CddEvaluationResult {\n");
    
    sb.append("  riskLevel: ").append(riskLevel).append("\n");
    sb.append("  riskScore: ").append(riskScore).append("\n");
    sb.append("  findings: ").append(findings).append("\n");
    sb.append("  riskModel: ").append(riskModel).append("\n");
    sb.append("  timestamp: ").append(timestamp).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
