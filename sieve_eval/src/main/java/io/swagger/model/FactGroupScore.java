package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;

import java.util.Date;


@ApiModel(description = "")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class FactGroupScore  {
  
  private String groupKey = null;
  private Double totalScore = null;
  private String computations = null;
  private Date timestamp = null;

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("group_key")
  public String getGroupKey() {
    return groupKey;
  }
  public void setGroupKey(String groupKey) {
    this.groupKey = groupKey;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("total_score")
  public Double getTotalScore() {
    return totalScore;
  }
  public void setTotalScore(Double totalScore) {
    this.totalScore = totalScore;
  }

  
  /**
   * A freeform, human-friendly description of computations that let to the total score.
   **/
  @ApiModelProperty(value = "A freeform, human-friendly description of computations that let to the total score.")
  @JsonProperty("computations")
  public String getComputations() {
    return computations;
  }
  public void setComputations(String computations) {
    this.computations = computations;
  }

  
  /**
   * Full date and time when the total score was computed.
   **/
  @ApiModelProperty(value = "Full date and time when the total score was computed.")
  @JsonProperty("timestamp")
  public Date getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class FactGroupScore {\n");
    
    sb.append("  groupKey: ").append(groupKey).append("\n");
    sb.append("  totalScore: ").append(totalScore).append("\n");
    sb.append("  computations: ").append(computations).append("\n");
    sb.append("  timestamp: ").append(timestamp).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
