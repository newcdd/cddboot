package io.swagger.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.*;

import java.util.ArrayList;
import java.util.List;


/**
 * A fact group represents a set of facts bundled together for evaluation. All facts in a group apply to one entity, e.g. a party or an account. Fact groups may differ in granularity. A fine-grained grouping may consist of facts related to a single risk factor. An example of a coarse-grained grouping is a complete KYA profile.
 **/
@ApiModel(description = "A fact group represents a set of facts bundled together for evaluation. All facts in a group apply to one entity, e.g. a party or an account. Fact groups may differ in granularity. A fine-grained grouping may consist of facts related to a single risk factor. An example of a coarse-grained grouping is a complete KYA profile.")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")
public class FactGroup  {
  
  private String groupKey = null;
  private List<String> factKeys = new ArrayList<String>();

  
  /**
   * The unique identifier for a fact group in the URN (www.wikipedia.org/wiki/Uniform_Resource_Name) format.\nFor example, for a sensible identifier for a group of party facts is a party key. Following URN convention, an RM_ID value of 123 may be represeneted as com:botw:rm:rm_id:123, a CM_ID value of 123 - as com:botw:bweCustMaster:cm_id:123, etc. The party identifier preferred by this service is AML GUID, a UUID value that should be passed like so: com:botw:aml:party:id:de305d54-75b4-431b-adb2-eb6b9e546014. \nSimilarly, a group of facts about an account (e.g. a KYA profile) is represented by an account number like so: com:botw:afs:acctNum:123.
   **/
  @ApiModelProperty(value = "The unique identifier for a fact group in the URN (www.wikipedia.org/wiki/Uniform_Resource_Name) format.\nFor example, for a sensible identifier for a group of party facts is a party key. Following URN convention, an RM_ID value of 123 may be represeneted as com:botw:rm:rm_id:123, a CM_ID value of 123 - as com:botw:bweCustMaster:cm_id:123, etc. The party identifier preferred by this service is AML GUID, a UUID value that should be passed like so: com:botw:aml:party:id:de305d54-75b4-431b-adb2-eb6b9e546014. \nSimilarly, a group of facts about an account (e.g. a KYA profile) is represented by an account number like so: com:botw:afs:acctNum:123.")
  @JsonProperty("group_key")
  public String getGroupKey() {
    return groupKey;
  }
  public void setGroupKey(String groupKey) {
    this.groupKey = groupKey;
  }

  
  /**
   **/
  @ApiModelProperty(value = "")
  @JsonProperty("fact_keys")
  public List<String> getFactKeys() {
    return factKeys;
  }
  public void setFactKeys(List<String> factKeys) {
    this.factKeys = factKeys;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class FactGroup {\n");
    
    sb.append("  groupKey: ").append(groupKey).append("\n");
    sb.append("  factKeys: ").append(factKeys).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
