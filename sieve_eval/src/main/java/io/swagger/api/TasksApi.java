package io.swagger.api;

import io.swagger.model.*;

import io.swagger.model.CddEvaluationTask;
import io.swagger.model.CddProfile;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

import static org.springframework.http.MediaType.*;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/*@Controller
@RequestMapping(value = "/task", produces = {APPLICATION_JSON_VALUE})
@Api(value = "/task", description = "the task API")
@javax.annotation.Generated(value = "class io.io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-22T21:35:23.669Z")*/
@RestController
@RequestMapping("/task")
public class TasksApi {


 /* @ApiOperation(value = "Schedules a task to evaluate the provided CDD Profile.", notes = "Submits a task to evaluate a `CddProfile` and returns immediately - before the evaluation has finished. A response message includes \n  * a task id\n  * an estimated wait time for completion\n  * a URL for tracking task progress and, upon completion, reading the result of the CDD evaluation.\n\nThis operation neither validates nor enriches input before processing (security scan excepted):\n  * party, account, and product keys are not verified.\n  * integrity and completeness of the `CddProfile` are examined during evaluation, and reported as part of the `CddEvaluationResult`.\n\nIf resources are available, the evaluation will begin immediately; otherwise, it will be scheduled and started as soon as resources become available. The system executes scheduled tasks in FIFO order, except for the following rules:\n  * Tasks submitted with the `timeliness` argument of _near real-time_ are started before _batch_.\n  * Tasks submitted by consumers that surpassed their quota (tracked per API key) are started last.", response = CddEvaluationTask.class, authorizations = {
          @Authorization(value = "basic_auth")
  })
  @ApiResponses(value = {
          @ApiResponse(code = 202, message = "The request for a CDD evaluation had been accepted for processing, but the processing has not been completed"),
          @ApiResponse(code = 400, message = "CDD profile not well-formed") })*/
  @RequestMapping(value = "/",produces = { "application/json" },consumes = { "application/json" },method = RequestMethod.POST)
  public ResponseEntity<CddEvaluationTask> tasksPost(
          /*
          @ApiParam(value = "A set of fact groups for evaluation." ,required=true ) CddProfile cddProfile
          ,
          @ApiParam(value = "Communicates a business need for the evaluation to occur in near real-time. \n\nComes into play only where there is contention for server resources. Under normal circumstances, all requests are handled in near real-time.", allowableValues = "{values=[nrt, batch]}", defaultValue = "batch") @RequestParam(value = "timeliness", required = false, defaultValue="batch") String timeliness
        */



  )
          throws NotFoundException {
    // do some magic!


    return new ResponseEntity<CddEvaluationTask>(HttpStatus.OK);
  }


}
