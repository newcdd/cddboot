package io.swagger.api;

import io.swagger.annotations.*;
import io.swagger.model.CddEvaluationResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static org.springframework.http.MediaType.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/*@Controller
@RequestMapping(value = "/evaluations", produces = {APPLICATION_JSON_VALUE})
@Api(value = "/evaluations", description = "the evaluations API")
@javax.annotation.Generated(value = "class io.swagger.codegen.languages.SpringMVCServerCodegen", date = "2015-12-23T00:50:04.537Z")*/
@RestController
@RequestMapping("/evaluations")
public class EvaluationsApi {
  

  @ApiOperation(value = "Get the result of the CDD evaluation by its task identifier", notes = "Returns a `CddEvaluationResult` that corresponds to the provided evaluation task id. \n\nThis operation potentially returns a lot of data, with `Findings` and `RiskScore` elements responsible for most of it. Consumers should use the `fields` parameter to request only the data necessitated by intended usage.\n\nThe `RiskScore` element requires elevated privileges: the requester has to be part of the CDD_BUCOS group in the Active Directory to see it.", response = CddEvaluationResult.class, authorizations = {
    @Authorization(value = "api_key")
  })
  @ApiResponses(value = { 
    @ApiResponse(code = 200, message = "CDD evaluation of a party"),
    @ApiResponse(code = 400, message = "Invalid task id"),
    @ApiResponse(code = 404, message = "No task found"),
    @ApiResponse(code = 200, message = "Unexpected error") })
  @RequestMapping(value = "/{eval_id}", 
    produces = { "application/json" }, 
    consumes = { "application/json" },
    method = RequestMethod.GET)
  public ResponseEntity<CddEvaluationResult> evaluationsEvalIdGet(
@ApiParam(value = "Unique identifier of the evaluation task.",required=true ) @PathVariable("evalId") String evalId

,
    @ApiParam(value = "Determines the portion of elements of a `CddEvaluationResult` that are returned. This parameter is a comma-separated list of fields, where each field is specified relative to the root of the response. See https://developers.google.com/drive/v2/web/performance#partial-response for more info.") @RequestParam(value = "fields", required = false) List<String> fields


)
      throws NotFoundException {
      // do some magic!
      return new ResponseEntity<CddEvaluationResult>(HttpStatus.OK);
  }

  
}
